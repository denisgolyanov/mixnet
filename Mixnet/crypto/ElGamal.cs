﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Math;
using Mixnet.math;

namespace Mixnet.crypto
{
    public class ElGamal
    {
        /**
         *	Randomly encrypts given element using El-Gamal encryption.
         *
         *  @param elem - the element to encrypt
         *  @param encryptionParams - parameters containing the group information, generator and public key.
         *  
         *  @return an ElGamalPair <G, M>, s.t. G = generator * r, M = elem + publicKey * r
         *          where r is a positive random in Zn, n = order(group). 
         */
        public static ElGamalPair encrypt(GroupElement elem,
                                          ElGamalEncryptionParameters encryptionParams)
        {
            BigInteger k;
            return reEncrypt(new ElGamalPair(encryptionParams.Group.getZero(), elem),
                             encryptionParams, out k);
        }

        /**
         *	Performs a re-encryption, using some positive random r in Zn, n = order(group)
         *  with respect to the given input <G, M> pair, using the given publicKey.
         */
        public static ElGamalPair reEncrypt(ElGamalPair inputPair,
                                            ElGamalEncryptionParameters encryptionParams, out BigInteger k)

        {
            Group group = encryptionParams.Group;
            k = GroupUtils.getRandomIntegerModulusGroupOrder(group, encryptionParams.Random);

            GroupElement G = encryptionParams.Generator.multiply(k).add(inputPair.G);
            GroupElement M = encryptionParams.PublicKey.multiply(k).add(inputPair.M);

            return new ElGamalPair(G, M);
        }

        /**
         *	Preforms decryption, based on given El-Gamal pair <G, M>, using the specified private key.
         */
        public static GroupElement decrypt(ElGamalPair pair,
                                           BigInteger privateKey)
        {
            GroupElement sharedSecret = pair.G.multiply(privateKey);
            return pair.M.subtract(sharedSecret);
        }

    }
}
