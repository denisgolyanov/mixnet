﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mixnet.math;
using Org.BouncyCastle.Math.EC;
using Mixnet.math.ec;

namespace Mixnet.crypto
{
    public class ElGamalPair
    {
        /**
         *	G - The encrypted random (g * r)
         *  M - The encrypted message (m + pk * r)
         */
        public ElGamalPair(GroupElement G, GroupElement M)
        {
            this.G = G;
            this.M = M;
        }

        public ElGamalPair(Meerkat.RerandomizableEncryptedMessage message, ECCurve curve) :
            this(new ECElement(toElGamalCipherTextMessage(message).C1.Data, curve),
                 new ECElement(toElGamalCipherTextMessage(message).C2.Data, curve))
        {

        }

        private static Meerkat.ElGamalCiphertext toElGamalCipherTextMessage(Meerkat.RerandomizableEncryptedMessage message)
        {
            return ProtoBuf.Serializer.Deserialize<Meerkat.ElGamalCiphertext>(new System.IO.MemoryStream(message.Data));
        }

        private static Meerkat.RerandomizableEncryptedMessage toRerandomizableEncrpytedMessage(Meerkat.ElGamalCiphertext message)
        {
            Meerkat.RerandomizableEncryptedMessage result = new Meerkat.RerandomizableEncryptedMessage();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            ProtoBuf.Serializer.Serialize<Meerkat.ElGamalCiphertext>(ms, message);
            result.Data = ms.ToArray();
            return result;
        }

        public Meerkat.RerandomizableEncryptedMessage getPairMessage()
        {
            Meerkat.ElGamalCiphertext elgamalCipherText = new Meerkat.ElGamalCiphertext();
            elgamalCipherText.C1 = new Meerkat.GroupElement();
            elgamalCipherText.C1.Data = this.G.encode();
            elgamalCipherText.C2 = new Meerkat.GroupElement();
            elgamalCipherText.C2.Data = this.M.encode();

            return toRerandomizableEncrpytedMessage(elgamalCipherText);
        }

        public GroupElement G;
        public GroupElement M; 
    }
}
