﻿using Mixnet.math;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mixnet.math.ec;
using Org.BouncyCastle.Math.EC;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto.Generators;

namespace Mixnet.crypto
{
    /**
     *	Encapsulates the public parameters used in the El-Gamal encryption protocol.
     *  the public key, usually denoted by 'y' = private_key * generator
     *  where "*" is multiplication in an additive group. Generator generates the group.
     */
    public class ElGamalEncryptionParameters
    {
        public ElGamalEncryptionParameters(GroupElement publicKey,
                                           GroupElement generator,
                                           SecureRandom random)
        {
            m_publicKey = publicKey;
            m_generator = generator;
            m_random = random;

            if (m_publicKey.getGroup() != generator.getGroup())
            {
                throw new ArgumentException("The public key and generator do not belong to the same group!");
            }
        }

        public GroupElement PublicKey
        {
            get { return m_publicKey; }
        }

        public GroupElement Generator
        {
            get { return m_generator; }
        }

        public SecureRandom Random
        {
            get { return m_random; }
        }

        public Group Group
        {
            get { return m_publicKey.getGroup(); }
        }

        public Meerkat.ElGamalPublicKey getPublicKeyMessage(String curveName)
        {
            if (!(this.PublicKey is ECElement))
            {
                throw new NotSupportedException("Key serialization is only support for EC groups");
            }

            // do not mistakenly use CreateSubjectPublicKeyInfo, it does not work correctly for named curves
            DerObjectIdentifier oid = CustomNamedCurves.GetOid(curveName);
            X962Parameters x962 = new X962Parameters(oid);
            ECPoint publicKeyPoint = ((ECElement)m_publicKey).getPoint();

            Asn1OctetString p = (Asn1OctetString)(new X9ECPoint(publicKeyPoint).ToAsn1Object());

            AlgorithmIdentifier algID = new AlgorithmIdentifier(
                X9ObjectIdentifiers.IdECPublicKey, x962.ToAsn1Object());

            SubjectPublicKeyInfo info = new SubjectPublicKeyInfo(algID, p.GetOctets());

            Meerkat.ElGamalPublicKey result = new Meerkat.ElGamalPublicKey();
            result.SubjectPublicKeyInfo = info.GetEncoded();
            return result;

        }

        private GroupElement m_publicKey;
        private GroupElement m_generator;
        private SecureRandom m_random;
    }
}
