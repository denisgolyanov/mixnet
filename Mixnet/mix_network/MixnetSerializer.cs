﻿using Mixnet.crypto;
using Mixnet.math.ec;
using Mixnet.proofs;
using Mixnet.math;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Math;
using System;

namespace Mixnet.mix_network
{
    public class MixnetSerializer
    {

        /**
         *	serializes a given mixnet to a file specified by the output path.
         *  The output format is as described by the .md file.
         */
        public static void serializeNetworkToFile(String outputPath, PermutationNetwork network)
        {
            using (System.IO.FileStream fileStream = System.IO.File.OpenWrite(outputPath))
            {
                ECCurve curve = ((ECGroup)network.getCipherTextTable()[0, 0].G.getGroup()).getCurve();

                Meerkat.MixBatchHeader header = new Meerkat.MixBatchHeader();
                header.Layers = network.getNumLayers();
                header.logN = (int)Math.Log(network.getNumCipherTextsInLayer(), 2);

                Console.WriteLine("Writing output file to: {0}", outputPath);
                Console.WriteLine("\tLogN: {0}, Layers: {1}", header.logN, header.Layers);

                ProtoBuf.Serializer.SerializeWithLengthPrefix<Meerkat.MixBatchHeader>(fileStream, header, ProtoBuf.PrefixStyle.Base128);


                // Go through the inputs of each layer + the output of the last layer
                for (int layer = 0; layer < header.Layers + 1; layer++)
                {
                    for (int ciphertextCount = 0; ciphertextCount < network.getNumCipherTextsInLayer(); ciphertextCount++)
                    {
                        Meerkat.RerandomizableEncryptedMessage pairMessage = network.getCipherTextTable()[layer, ciphertextCount].getPairMessage();
                        ProtoBuf.Serializer.SerializeWithLengthPrefix<Meerkat.RerandomizableEncryptedMessage>(fileStream, pairMessage, ProtoBuf.PrefixStyle.Base128);
                    }
                }

                // Go through the layers
                for (int layer = 0; layer < header.Layers; layer++)
                {
                    // Go through the switches in each layer
                    for (int switchIndex = 0; switchIndex < network.getNumCipherTextsInLayer() / 2; switchIndex++)
                    {
                        Meerkat.Mix2Proof proof = network.getProofsTable()[layer, switchIndex].getProtobufMessage((uint)switchIndex, (uint)layer,
                                                                                                      (uint)network.getMapTable()[layer, switchIndex * 2],
                                                                                                      (uint)network.getMapTable()[layer, switchIndex * 2 + 1]);
                        ProtoBuf.Serializer.SerializeWithLengthPrefix<Meerkat.Mix2Proof>(fileStream, proof, ProtoBuf.PrefixStyle.Base128);

                    }
                }
            }
        }

        /**
         *	Deserializes a mixnet, according to the format described in the .md file, from a given file.
         * 
         *  @return a PermutationNetwork, containing all the encryptions and switching proofs.
         *          the network is ready for verification.
         */
        public static PermutationNetwork parseMixnet(ElGamalEncryptionParameters encryptParams, String proverOutputPath)
        {
            Console.WriteLine("Parsing mixnet proof at path: {0}", proverOutputPath);

            using (System.IO.FileStream fileStream = System.IO.File.OpenRead(proverOutputPath))
            {
                Meerkat.MixBatchHeader header = ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.MixBatchHeader>(fileStream, ProtoBuf.PrefixStyle.Base128);
                Console.WriteLine("\tLayers = {0}, logN = {1}", header.Layers, header.logN);

                int numCipherTexts = (int)Math.Pow(2, header.logN);
                int numSwitches = numCipherTexts / 2;

                ElGamalPair[,] cipherTexts = new ElGamalPair[header.Layers + 1, numCipherTexts];
                SwitchProof[,] switchProofs = new SwitchProof[header.Layers, numSwitches];
                int[,] outputIndices = new int[header.Layers, numCipherTexts];
                ECCurve curve = ((ECGroup)encryptParams.Group).getCurve();

                // Go through the inputs of each layer + the output of the last layer
                for (int layer = 0; layer < header.Layers + 1; layer++)
                {
                    // Go through the inputs of a specific layer (or the output of the mixnet, in case of the last iteration)
                    for (int ciphertextCount = 0; ciphertextCount < numCipherTexts; ciphertextCount++)
                    {
                        Meerkat.RerandomizableEncryptedMessage pairMessage =
                                        ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.RerandomizableEncryptedMessage>(fileStream, ProtoBuf.PrefixStyle.Base128);
                        cipherTexts[layer, ciphertextCount] = new ElGamalPair(pairMessage, curve);
                    }
                }

                // Go through the layers
                for (int layer = 0; layer < header.Layers; layer++)
                {
                    // Go through the switches in each layer
                    for (int switchIndex = 0; switchIndex < numSwitches; switchIndex++)
                    {
                        Meerkat.Mix2Proof proof =
                            ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.Mix2Proof>(fileStream, ProtoBuf.PrefixStyle.Base128);

                        switchProofs[layer, switchIndex] = new SwitchProof(proof, curve);
                        outputIndices[layer, switchIndex * 2] = proof.location.Out0;
                        outputIndices[layer, switchIndex * 2 + 1] = proof.location.Out1;
                    }
                }

                return new PermutationNetwork(encryptParams, cipherTexts, switchProofs, outputIndices);
            }
        }

        /**
         *	Loads the key information, and group information from the key file at given path.
         *  The key file format is as specified by the .md file.
         */
        public static ElGamalEncryptionParameters loadKeyInformation(String keyInputPath)
        {

            ECCurve curve;
            ECPoint publicKey;
            ECPoint generator;
            BigInteger privateKey;

            using (System.IO.FileStream fileStream = System.IO.File.OpenRead(keyInputPath))
            {
                Meerkat.ElGamalPublicKey pubKey = ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.ElGamalPublicKey>(fileStream, ProtoBuf.PrefixStyle.Base128);
                Meerkat.BigInteger privateKeyMessage = ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.BigInteger>(fileStream, ProtoBuf.PrefixStyle.Base128);
                privateKey = new BigInteger(1, privateKeyMessage.Data);

                AsymmetricKeyParameter asymKeyParams = PublicKeyFactory.CreateKey(pubKey.SubjectPublicKeyInfo);

                ECPublicKeyParameters ecParams = (ECPublicKeyParameters)asymKeyParams;
                ECDomainParameters ecDomainParams = ecParams.Parameters;

                curve = ecDomainParams.Curve;
                generator = ecDomainParams.G;
                publicKey = ecParams.Q;

                Console.WriteLine("Key information loaded successfully, algorithm: {0}, curve: {1}", ecParams.AlgorithmName, curve);
            }

            ECGroup group = new ECGroup(curve);
            GroupElement generatorElement = new ECElement(generator, group);
            GroupElement publicKeyElement = new ECElement(publicKey, group);

            return new ElGamalEncryptionParameters(publicKeyElement, generatorElement, new SecureRandom());
        }
    }
}
