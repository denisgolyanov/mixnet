﻿using Mixnet.crypto;
using Mixnet.math.ec;
using Org.BouncyCastle.Math.EC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.mix_network
{
    public class MixnetCreator
    {
        /**
        *  Creates a mixnet with given inputs.
        *  The function performs the mixing operations and generates ZK proofs for each switch operation.
        * 
        *  @param encryptParams encryption parameters
        *  @param encrpytedInputsFilePath path to a file that contains the encrypted inputs to mix.
        *
        *  @return the generated mix network.
        */
        public static PermutationNetwork createMixnet(ElGamalEncryptionParameters encryptParams, String encryptedInputsFilePath)
        {
            List<ElGamalPair> cipherTextsList;
            int numEncryptions;

            ECCurve curve = ((ECGroup)encryptParams.Group).getCurve();
            using (System.IO.FileStream fileStream = System.IO.File.OpenRead(encryptedInputsFilePath))
            {

                Meerkat.MixBatchHeader header =
                                    ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.MixBatchHeader>(fileStream, ProtoBuf.PrefixStyle.Base128);
                cipherTextsList = new List<ElGamalPair>();
                // Go through the inputs of a specific layer (or the output of the mixnet, in case of the last iteration)
                for (int ciphertextCount = 0; fileStream.Position < fileStream.Length; ciphertextCount++)
                {
                    Meerkat.RerandomizableEncryptedMessage pairMessage =
                                    ProtoBuf.Serializer.DeserializeWithLengthPrefix<Meerkat.RerandomizableEncryptedMessage>(fileStream, ProtoBuf.PrefixStyle.Base128);
                    cipherTextsList.Add(new ElGamalPair(pairMessage, curve));
                }
            }

            numEncryptions = cipherTextsList.Count;
            BenesNetwork benes = new BenesNetwork(numEncryptions);
            Random rnd = new Random();

            int[] indices = new int[numEncryptions];
            int[] desiredPermute = new int[numEncryptions];
            for (int j = 0; j < numEncryptions; j++)
            {
                indices[j] = j;

                int tmp = j;
                int randomIndex = rnd.Next(j + 1);
                desiredPermute[j] = desiredPermute[randomIndex];
                desiredPermute[randomIndex] = tmp;
            }

            benes.setPermutation(indices, desiredPermute, 0, 0);

            PermutationNetwork permNetwork = new PermutationNetwork(encryptParams, benes.getLayers(), cipherTextsList);
            permNetwork.computeNetwork(benes);
            return permNetwork;
        }

    }
}
