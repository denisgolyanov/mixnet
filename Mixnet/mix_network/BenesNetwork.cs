﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.mix_network
{

    public class BenesNetwork
    {
        int inputLength;
        Boolean[,] crossSG; //switchgate matrix, true if the sg is croosed, false otherwise
        int logInput;

        public BenesNetwork(int inputLength)
        {
            this.inputLength = inputLength;
            this.logInput = (int)Math.Log(inputLength, 2);
            this.crossSG = new bool[2 * logInput - 1, inputLength - 1];
        
        }


        public Boolean[,] getCrossSG() { return this.crossSG; }
        public int getInputLength() { return this.inputLength; }
        public int getLayers() {return 2 * this.logInput - 1;}


        /**
        *	
        *  auxilary function to find the column index in i row that hold value in map table
        *  @param i - the row in table to search
        *  @param value - which value to search
        *  @ mapTable  - the table to search in
        *  @return the desired index
        *          
        */
        private int findIndex(int val, int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)          
            {
                if (arr[i] == val)                
                    return i;                
            }
            return -1;
        }


        /**
        *	
        *  get the index in the next layer by the algorithem
        *  @param logInput - log of the input size
        *  @param layer - the current layer
        *  @ inputIndex  - the current index
        *  @return the index in the next layer
        *          
        */
        public int getNextIndex(int logInput, int layer, int inputIndex)
        {

            int cross;

            if ((layer == 0) || (layer == 2 * logInput - 1) || ((inputIndex % 2) == 0))
            {
                return inputIndex;
            }


            if (layer >= logInput)
            {
                cross = layer - logInput + 1;
            }

            else 
            {
                cross = logInput - layer;
            }
            return inputIndex ^ 1 << cross;
        }





        /**
       *	
       *  set the switchgate cross status 
       *  @param input - input array - the init permute
       *  @param output - output array - the desired permute
       *  @ index  - the current index
       *  @ level  - the level of the recursion 
       *  
       *          
       */
        public void setPermutation(int[] input, int[] output, int index, int level)
        {
            
            int aux = 1 << this.logInput - level - 1;
            if (level == this.logInput - 1)
            {
                if (input[0] == output[0])
                {
                    this.crossSG[level, index] = false;
                }
                else
                {
                    this.crossSG[level, index] = true; //set cross
                }
                return;
            }

            int[] inputPartA = new int[output.Length / 2];
            int[] inputPartB = new int[output.Length / 2];

            int[] outputPartA = new int[output.Length / 2];
            int[] outputPartB = new int[output.Length / 2];

            //sort the input
            SortedSet<int> unmatched = new SortedSet<int>();
            for (int i = 0; i < input.Length; i++)
            {
                unmatched.Add(i);
            }

            int startIndex = index << this.logInput - level - 1;
            int numSwitches = 1 << this.logInput - level - 1;
            while (unmatched.Count != 0) //adjust the sg in order to get the permute
            {
                int i = ((int?)unmatched.First()).Value;
                do
                {
                    unmatched.Remove(i);
                    unmatched.Remove(i ^ 0x1);

                    int switchNum =(i >> 1);
                    int cross;

                    if ((((i & 0x1) == 0) && (switchNum < numSwitches / 2)) || (((i & 0x1) == 1) && (switchNum >= numSwitches / 2)))
                    {
                        this.crossSG[(2 * this.logInput - level - 2), (startIndex + switchNum)] = false;
                        cross = i;
                    }
                    else
                    {
                        this.crossSG[(2 * this.logInput - level - 2), (startIndex + switchNum)] = true;
                        cross = i ^ 0x1;
                    }
                    int nextIndex = getNextIndex(this.logInput - level, 2 * (this.logInput - level) - 2, cross);
                    int nextPairIndex = getNextIndex(this.logInput - level, 2 * (this.logInput - level) - 2, cross ^ 0x1);

                    outputPartA[nextIndex] = output[i];
                    outputPartB[(nextPairIndex - aux)] = output[(i ^ 0x1)];

                    int j = findIndex(output[i], input);
                    int sgIndex = (j >> 1);
                    int crossJ;

                    if ((((j & 0x1) == 0) && (sgIndex < numSwitches / 2)) || (((j & 0x1) == 1) && (sgIndex >= numSwitches / 2)))
                    {
                        this.crossSG[level, (startIndex + sgIndex)] = false;
                        crossJ = j;
                    }
                    else
                    {
                        this.crossSG[level, (startIndex + sgIndex)] = true;
                        crossJ = j ^ 0x1;
                    }
                    int pair = getNextIndex(this.logInput - level, 1, crossJ);


                    inputPartA[pair] = output[i];

                    int nextPairJ = getNextIndex(this.logInput - level, 1, crossJ ^ 0x1);
                    inputPartB[(nextPairJ - aux)] = input[(j ^ 0x1)];

                    int nextPairI = findIndex(input[(j ^ 0x1)], output);
                    i = nextPairI ^ 0x1;
                } while (unmatched.Contains(i));
            }
            setPermutation(inputPartA, outputPartA, 2 * index, level + 1);
            setPermutation(inputPartB, outputPartB, 2 * index + 1, level + 1);
        }

    }
}