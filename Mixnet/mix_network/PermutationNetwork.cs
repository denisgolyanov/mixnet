﻿using Mixnet.crypto;
using Mixnet.proofs;
using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mixnet.mix_network
{

    /**
     *	Represents the mixnet network, as a collection of arrays. 
     *  Stores the ciphertext and proof matrices and the connections between switch gates in consecutive layers.
     *  for each switch outputs, stores the indices of their corresponding inputs in the next layer.
     */
    public class PermutationNetwork
    {
        /*
        *  
        *  Use this constructor when creating a (new) mixnet from a single layer of inputs. 
        *  
        *  @Param encryptParams - encryption parameters
        *  @param numLayers - number of layers in the mixnet
        *  @param firstLayerCipherTexts  - input for the first layer
        *  
        *  @throws exception if the number of ciphertexts (in a layer) is not a power of 2
        *          
        */
        public PermutationNetwork(ElGamalEncryptionParameters encryptParams, 
                                  int numLayers,
                                  List<ElGamalPair> firstLayerCipherTexts)
        {
            this.m_encryptParams = encryptParams;
            this.m_numCiphertextsInLayer = firstLayerCipherTexts.Count;
            this.m_layers = numLayers;            
            this.m_cipherTextTable = new ElGamalPair[numLayers + 1, firstLayerCipherTexts.Count];

            if (0 != (m_numCiphertextsInLayer & (m_numCiphertextsInLayer - 1)))
            {
                throw new InvalidOperationException("Number of ciphertexts in each layer must be a power of two (given: " + this.m_numCiphertextsInLayer + ")");
            }

            // initialize first layer of the cipher text matrix.
            for (int j = 0; j < m_numCiphertextsInLayer; j++)
            {
                m_cipherTextTable[0, j] = firstLayerCipherTexts[j];
            }
        }

        /**
         *	Reconstruct an existing mixnet.
         *  Note that in this type of construction, the network may only be verified for correntess. 
         *
         *  @param encryptParams encryption parameters
         *  @param cipherTexts a [#layers+1, #numCipherTexts] sized matrix of ciphertexts.
         *  @param switchProofs a [#layers, #numSwitches] sized matrix of proofs.
         *  @Param outputIndices a [#layers, #numCipherTexts] sized matrix, where element [l, k] stores
         *                       the index of the consecutive input connected to the output at layer l, index k.
         *                       also referred to as the map table.
         *                                     
         */
        public PermutationNetwork(ElGamalEncryptionParameters encryptParams, ElGamalPair[,] cipherTexts, SwitchProof[,] switchProofs, int[,] outputIndices)
        {
            this.m_encryptParams = encryptParams;
            this.m_numCiphertextsInLayer = cipherTexts.GetLength(1);
            this.m_proofsTable = switchProofs;
            this.m_mapTable = outputIndices;
            this.m_layers = switchProofs.GetLength(0);
            this.m_cipherTextTable = cipherTexts;
        }

        public ElGamalPair[,] getCipherTextTable()
        {
            return m_cipherTextTable;
        }

        public int getNumLayers()
        {
            return m_layers;
        }

        public SwitchProof[,] getProofsTable()
        {
            return m_proofsTable;
        }

        public int[,] getMapTable()
        {
            return m_mapTable;
        }

        public int getNumCipherTextsInLayer()
        {
            return m_numCiphertextsInLayer;
        }

         /**
         * Initializes the map table with gates-input indices according to Benes network physical connections
         * @param net - Benes Network object - stores the information regarding physical connections.
         *          
         */
        public void initializeMapTable(BenesNetwork net)
        {
            m_mapTable = new int[m_layers, m_numCiphertextsInLayer];

            for (int i = 0; i < m_layers; i++)
            {
                for (int j = 0; j < m_numCiphertextsInLayer; j++)
                {
                    m_mapTable[i, j] = net.getNextIndex((int)Math.Log(m_numCiphertextsInLayer, 2), i+1, j);
                }
            }
        }

        /**
        *  Performs the main operation of the mixnet.
        *
        *  Mixes the inputs, starting from the first layer, using the switch gates
        *  till reaches the outputs at the last layer.
        *  For each switch operation, a proof is generated.
        *
        *  @param crossInformationNetwork - stores the information regarding whether each switch should perform a "cross" operation.
        *                                   also contains information regarding the physical connections between the layers.
        *                                   (i.e. which output is mapped to which input in the next layer).
        *                                   
        */
        public void computeNetwork(BenesNetwork crossInformationNetwork)
        {
            if (m_proofsTable != null)
            {
                throw new InvalidOperationException("The network has already been computed!");
            }

            this.initializeMapTable(crossInformationNetwork);

            m_proofsTable = new SwitchProof[m_layers, m_numCiphertextsInLayer / 2];

            for (int layer = 0; layer < m_layers; layer++)
            {
                for (int switchIdx = 0; switchIdx < (m_numCiphertextsInLayer / 2); switchIdx++) 
                {

                    ElGamalPair m_inputOne = m_cipherTextTable[layer, 2 * switchIdx];
                    ElGamalPair m_inputTwo = m_cipherTextTable[layer, 2 * switchIdx + 1];

                    ElGamalPair m_outputone = m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx]];
                    ElGamalPair m_outputTwo = m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx + 1]];

                    SwitchingGate gate = new SwitchingGate(m_encryptParams);
                    gate.InputOne = m_inputOne;
                    gate.InputTwo = m_inputTwo;

                    SwitchProof proof = gate.doOperation(crossInformationNetwork.getCrossSG()[layer,switchIdx]);
                    m_proofsTable[layer, switchIdx] = proof;

                    m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx]] = gate.OutputOne;
                    m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx + 1]] = gate.OutputTwo;
                }
            }
        }

        /**
         *	Performs the main verifier operation.
         *
         *  Verifiers that each switch operation was performed as expected.
         */
        public bool[,] verifySwitchGateProofs()
        {
            if (m_proofsTable == null)
            {
                throw new InvalidOperationException("The network has not been proved yet!");
            }

            bool[,] verifTable = new bool[m_layers, m_numCiphertextsInLayer / 2];

            for (int layer = 0; layer < m_layers; layer++)
            {
                for (int switchIdx = 0; switchIdx < (m_numCiphertextsInLayer / 2); switchIdx++)
                {
                    ElGamalPair m_inputOne = m_cipherTextTable[layer, 2 * switchIdx];
                    ElGamalPair m_inputTwo = m_cipherTextTable[layer, 2 * switchIdx + 1];

                    ElGamalPair m_outputone = m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx]];
                    ElGamalPair m_outputTwo = m_cipherTextTable[layer + 1, m_mapTable[layer, 2 * switchIdx + 1]];

                    Verifier verifier = new Verifier(m_encryptParams);

                    verifTable[layer, switchIdx] =
                        verifier.verify(m_inputOne, m_inputTwo, m_outputone, m_outputTwo, m_proofsTable[layer, switchIdx]);
                }
            }
            return verifTable;
        }

        /**
         *	Element [l, k] stores the switch operation proof for the switch gate
         *  at layer l, index k.
         */
        private SwitchProof[,] m_proofsTable;

        /**
         *	Element [l, k] stores the cipher text at layer l, index k.
         *  (the size of the matrix is (#layers + 1) x #numCipherTexts)
         *  The first layer in this case represents the inputs to the mixnet.
         */
        private ElGamalPair[,] m_cipherTextTable;        
        private int m_layers;
        private int m_numCiphertextsInLayer;

        /**
         * a [#layers, #numCipherTexts] sized matrix, where element [l, k] stores
         * the index of the consecutive input connected to the output at layer l, index k.
         */
        private int[,] m_mapTable;
        private ElGamalEncryptionParameters m_encryptParams;
    }
}
