﻿using Mixnet.crypto;
using Mixnet.proofs;
using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.mix_network
{
    public class SwitchingGate
    {
        public SwitchingGate(ElGamalEncryptionParameters encryptionParams)
        {
            m_params = encryptionParams;
            m_inputOne = m_inputTwo = null;
            m_outputOne = m_outputTwo = null;
        }

        /**
         *	Executes the task of the switching gate.
         *  Each one of the two possible inputs must be set prior to calling this method.
         *  The outputs are randomized with probability 0.5, regardless of other switch gates in the network.
         *
         *  @param cross whether the current switch should perform a "cross"
         *               in this case the first input is mapped to the second output.
         */
        public SwitchProof doOperation(bool cross)
        {
            if (m_outputOne != null || m_outputTwo != null)
            {
                throw new InvalidOperationException("The switch gate operation has already been executed");
            }

            if (m_inputOne == null || m_inputTwo == null)
            {
                throw new InvalidOperationException("The switch gate has not received both of its inputs");
            }

            BigInteger dlog0, dlog1;
            ElGamalPair encryptedOne = ElGamal.reEncrypt(m_inputOne, m_params, out dlog0);
            ElGamalPair encryptedTwo = ElGamal.reEncrypt(m_inputTwo, m_params, out dlog1);

            if (cross)
            {
                m_outputOne = encryptedTwo;
                m_outputTwo = encryptedOne;
            }
            else
            {
                m_outputOne = encryptedOne;
                m_outputTwo = encryptedTwo;
            }

            Prover switchProver = new Prover(m_params);
            return switchProver.proveSwitch(m_inputOne, m_inputTwo,
                                            encryptedOne, encryptedTwo, dlog0, dlog1,
                                            cross); 
        }

        public ElGamalPair InputOne
        {
            set
            {
                m_inputOne = value;
            }
        }

        public ElGamalPair InputTwo
        {
            set
            {
                m_inputTwo = value;
            }
        }

        public ElGamalPair OutputOne
        {
            get
            {
                if (m_outputOne == null)
                {
                    throw new InvalidOperationException("SwitchGate operation was not preformed before accessing output");
                }
                return m_outputOne;
            }
        }

        public ElGamalPair OutputTwo
        {
            get
            {
                if (m_outputTwo == null)
                {
                    throw new InvalidOperationException("SwitchGate operation was not preformed before accessing output");
                }
                return m_outputTwo;
            }
        }

        ElGamalEncryptionParameters m_params;

        ElGamalPair m_inputOne;
        ElGamalPair m_inputTwo;

        ElGamalPair m_outputOne;
        ElGamalPair m_outputTwo;
    }
}
