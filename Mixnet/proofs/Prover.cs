﻿using Mixnet.crypto;
using Mixnet.math;
using Org.BouncyCastle.Math;

namespace Mixnet.proofs
{
    /**
     *	Houses the ZK prover logic for the operation performed by a switch gate.
     */
    public class Prover
    {
        public Prover(ElGamalEncryptionParameters encryptParams)
        {
            m_eParams = encryptParams;
        }

        /**
         *	Generates a proof for the switch operation. 
         *  
         *  @param in0, in1 - The inputs to the switch
         *  @param encryptedIn0, encryptedIn1 - The re-encryption outputs for in0, in1 (without switching)
         *  @param dlog0, dlog1 - The randoms used in the re-encryptions, respectively.
         *  @param mix - if true, the first input is mapped to the second output by the switching gate.
         *               in this case, the proof for the first conjunction is simulated.
         *
         */
        public SwitchProof proveSwitch(ElGamalPair in0, ElGamalPair in1,
                                       ElGamalPair encryptedIn0, ElGamalPair encryptedIn1,
                                       BigInteger dlog0, BigInteger dlog1, bool mix)
        {
            m_mix = mix;
            BigInteger fakeChallenge, realChallenge;
            AndProof simulatedProof = simulateIncorrectStatement(in0, in1, encryptedIn0, encryptedIn1, out fakeChallenge);
            AndProof reencryptionProof = proveReencryption(dlog0, dlog1, fakeChallenge, out realChallenge, simulatedProof);

            if (m_mix == true)
            {
                return new SwitchProof(simulatedProof, reencryptionProof, fakeChallenge);
            }
            else
            {
                return new SwitchProof(reencryptionProof, simulatedProof, realChallenge);
            }
        }

        private DlogProof simulateDlogProof(BigInteger challenge, GroupElement powerOfG, GroupElement powerOfH)
        {

            BigInteger xcr = GroupUtils.getRandomIntegerModulusGroupOrder(m_eParams.Group, m_eParams.Random);

            GroupElement gr = m_eParams.Generator.multiply(xcr).subtract(powerOfG.multiply(challenge)); // g^xcr/(powerOfG^c)
            GroupElement hr = m_eParams.PublicKey.multiply(xcr).subtract(powerOfH.multiply(challenge)); // h^xcr/(powerOfH^c)

            return new DlogProof(new DlogProof.FirstMsg(gr, hr),
                                 new DlogProof.FinalMsg(xcr));
        }

        private AndProof simulateIncorrectStatement(ElGamalPair in0, ElGamalPair in1, ElGamalPair encryptedIn0, ElGamalPair encryptedIn1, out BigInteger challenge)
        {
            // In this statement, values of powerOfG and powerOfH for each dlog equivalence statement
            // are computed such that input0 is mapped to output1 and input1 is mapped to output0
            AndProofStatements incorrectAndStatement = new Mix2Statements(in0, in1, encryptedIn0, encryptedIn1).clause1;

            challenge = GroupUtils.getRandomIntegerModulusGroupOrder(m_eParams.Group, m_eParams.Random);

            DlogProof clause0 = simulateDlogProof(challenge,
                                                  incorrectAndStatement.clause0.powerOfG,
                                                  incorrectAndStatement.clause0.powerOfH);
            DlogProof clause1 = simulateDlogProof(challenge,
                                                  incorrectAndStatement.clause1.powerOfG,
                                                  incorrectAndStatement.clause1.powerOfH);

            return new AndProof(clause0, clause1);
        }

        private DlogProof.FirstMsg createDlogProofFirstMessage(out BigInteger rand)
        {
            rand = GroupUtils.getRandomIntegerModulusGroupOrder(m_eParams.Group, m_eParams.Random);

            // gr = g ^ r
            // hr = h ^ r
            GroupElement gr = m_eParams.Generator.multiply(rand);
            GroupElement hr = m_eParams.PublicKey.multiply(rand);

            return new DlogProof.FirstMsg(gr, hr);
        }

        private DlogProof.FinalMsg createDlogProofFinalMessage(BigInteger dlog, BigInteger firstMessageRand, BigInteger challenge)
        {
            // calculating the xcr for the final message
            // x = the dlog answer (the random from the reEncrypt)
            // c = the challenge
            // r = the firstMessageRand 
            // calculating: x*c + r

            BigInteger xcr = challenge.Multiply(dlog).Add(firstMessageRand);
            return new DlogProof.FinalMsg(xcr);
        }

        private SwitchProof createFirstMessageProof(AndProof realProof, AndProof simulatedProof)
        {
            SwitchProof res;
            if (m_mix == true)
            {
                res = new SwitchProof(simulatedProof, realProof, null);
            }
            else
            {
                res = new SwitchProof(realProof, simulatedProof, null);
            }
            return res;
        }

        private AndProof proveReencryption(BigInteger dlog0, BigInteger dlog1, BigInteger fakeChallenge, out BigInteger realChallenge, AndProof simulatedProof)
        {
            BigInteger rand0, rand1;
            DlogProof.FirstMsg clause0FirstMessage = createDlogProofFirstMessage(out rand0);
            DlogProof.FirstMsg clause1FirstMessage = createDlogProofFirstMessage(out rand1);

            SwitchProof sp = createFirstMessageProof(new AndProof(new DlogProof(clause0FirstMessage, null),
                                                                  new DlogProof(clause1FirstMessage, null)), 
                                                            simulatedProof);

            BigInteger oracleChallenge = FiatShamir.computeOracleChallenge(sp);
            realChallenge = oracleChallenge.Subtract(fakeChallenge).Mod(m_eParams.Group.getOrder());

            DlogProof.FinalMsg clause0FinalMessage = createDlogProofFinalMessage(dlog0, rand0, realChallenge);
            DlogProof.FinalMsg clause1FinalMessage = createDlogProofFinalMessage(dlog1, rand1, realChallenge);

            return new AndProof(new DlogProof(clause0FirstMessage, clause0FinalMessage),
                                new DlogProof(clause1FirstMessage, clause1FinalMessage));
        }

        private ElGamalEncryptionParameters m_eParams;
        private bool m_mix;
    }
}
