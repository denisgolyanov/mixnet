﻿using Mixnet.crypto;
using Mixnet.math;

namespace Mixnet.proofs
{

    /**
    *  A statement that log_g(powerOfG) = log_h(powerOfH) = r
    *  where powerOfG = outputG / inputG, powerOfH = outputM / inputM, g is the generator and h is the public key.
    */
    public class DlogStatement
    {
        public DlogStatement(ElGamalPair input, ElGamalPair output)
        {
            powerOfG = output.G.subtract(input.G);
            powerOfH = output.M.subtract(input.M);
        }

        // a power of the generator g
        public GroupElement powerOfG;

        // a power of the public key h
        public GroupElement powerOfH;
    }

    /**
    *  A Statement that om1, om2 were generated from im1, om2 in the following manner:
    *  let im1 = <G, M>, im2 = <G', M'>, then om1 = <G + g * r, M + h * r>, om2 = <G' + g * r', M' + h * r'>
    *  where g is the generator, h is the public key and r, r' are some random integers. 
    */
    public class AndProofStatements
    {
        public AndProofStatements(ElGamalPair im1, ElGamalPair im2, ElGamalPair om1, ElGamalPair om2)
        {
            clause0 = new DlogStatement(im1, om1);
            clause1 = new DlogStatement(im2, om2);
        }

        public DlogStatement clause0;
        public DlogStatement clause1;
    }


    /**
     *	A statement that om1, om2 are switch-gate outputs of corresponding inputs im1, im2
     *  OR that om2, om1 are switch-gate outputs of corresponding inputs im1, im2. 
     */
    public class Mix2Statements
    {
        public Mix2Statements(ElGamalPair im1, ElGamalPair im2, ElGamalPair om1, ElGamalPair om2)
        {
            clause0 = new AndProofStatements(im1, im2, om1, om2);
            clause1 = new AndProofStatements(im1, im2, om2, om1);
        }

        public AndProofStatements clause0;
        public AndProofStatements clause1;
    }
}
