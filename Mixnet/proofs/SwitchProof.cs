﻿using Mixnet.math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Math;
using Mixnet.math.ec;
using Org.BouncyCastle.Math.EC;

namespace Mixnet.proofs
{

    public class DlogProof
    {
        public DlogProof(FirstMsg firstMessage, FinalMsg finalMessage)
        {
            this.firstmsg = firstMessage;
            this.finalmsg = finalMessage;
        }

        public DlogProof(Meerkat.Mix2Proof.DlogProof.FirstMessage firstMessage,
                         Meerkat.Mix2Proof.DlogProof.FinalMessage finalMessage,
                         ECCurve curve)
            : this(new FirstMsg(firstMessage, curve), new FinalMsg(finalMessage))
        { 
        }

        public Meerkat.Mix2Proof.DlogProof.FirstMessage getFirstMessage()
        {
            Meerkat.Mix2Proof.DlogProof.FirstMessage firstMessage = new Meerkat.Mix2Proof.DlogProof.FirstMessage();
            firstMessage.Gr = new Meerkat.GroupElement();
            firstMessage.Gr.Data = this.firstmsg.gr.encode();

            firstMessage.Hr = new Meerkat.GroupElement();
            firstMessage.Hr.Data = this.firstmsg.hr.encode();

            return firstMessage;
        }

        public Meerkat.Mix2Proof.DlogProof.FinalMessage getFinalMessage()
        {
            Meerkat.Mix2Proof.DlogProof.FinalMessage finalMessage = new Meerkat.Mix2Proof.DlogProof.FinalMessage();
            finalMessage.Xcr = new Meerkat.BigInteger();
            finalMessage.Xcr.Data = this.finalmsg.xcr.ToByteArray();
            return finalMessage;
        }

        public class FirstMsg
        {
            public FirstMsg(GroupElement gr, GroupElement hr)
            {
                this.gr = gr;
                this.hr = hr;
            }

            public FirstMsg(Meerkat.Mix2Proof.DlogProof.FirstMessage firstMessage, ECCurve curve)
            {
                this.gr = new ECElement(firstMessage.Gr.Data, curve);
                this.hr = new ECElement(firstMessage.Hr.Data, curve);
            }

            public GroupElement gr;
            public GroupElement hr;
        }

        public class FinalMsg
        {
            public FinalMsg(BigInteger xcr)
            {
                this.xcr = xcr;
            }

            public FinalMsg(Meerkat.Mix2Proof.DlogProof.FinalMessage finalMessage)
            {
                this.xcr = new BigInteger(1, finalMessage.Xcr.Data);
            }

            public BigInteger xcr;
        }
        public FirstMsg firstmsg;
        public FinalMsg finalmsg;
    }

    public class AndProof
    {

        public AndProof(DlogProof clause0, DlogProof clause1)
        {
            this.clause0 = clause0;
            this.clause1 = clause1;
        }

        public AndProof(Meerkat.Mix2Proof.AndProof.FirstMessage firstMessage,
                        Meerkat.Mix2Proof.AndProof.FinalMessage finalMessage,
                        ECCurve curve) : 
            this(new DlogProof(firstMessage.Clause0, finalMessage.Clause0, curve),
                 new DlogProof(firstMessage.Clause1, finalMessage.Clause1, curve))
        {

        }


        public Meerkat.Mix2Proof.AndProof.FirstMessage getFirstMessage()
        {
            Meerkat.Mix2Proof.AndProof.FirstMessage result = new Meerkat.Mix2Proof.AndProof.FirstMessage();
            result.Clause0 = this.clause0.getFirstMessage();
            result.Clause1 = this.clause1.getFirstMessage();
            return result;
        }

        public Meerkat.Mix2Proof.AndProof.FinalMessage getFinalMessage()
        {
            Meerkat.Mix2Proof.AndProof.FinalMessage result = new Meerkat.Mix2Proof.AndProof.FinalMessage();
            result.Clause0 = this.clause0.getFinalMessage();
            result.Clause1 = this.clause1.getFinalMessage();
            return result;
        }

        public DlogProof clause0;
        public DlogProof clause1;
    }

    // a switch proof container
    public class SwitchProof
    {

        public SwitchProof(AndProof clause0Proof, AndProof clause1Proof, BigInteger clause0Challenge)
        {
            this.clause0 = clause0Proof;
            this.clause1 = clause1Proof;
            this.challenge = clause0Challenge;
        }

        public SwitchProof(Meerkat.Mix2Proof proofMessage, ECCurve curve)
            : this(new AndProof(proofMessage.firstMessage.Clause0, proofMessage.finalMessage.Clause0, curve),
                   new AndProof(proofMessage.firstMessage.Clause1, proofMessage.finalMessage.Clause1, curve),
                   new BigInteger(1, proofMessage.finalMessage.C0.Data))
        {
            m_originalMessage = proofMessage.firstMessage;
        }

        public Meerkat.Mix2Proof.FirstMessage getFirstMessage()
        {
            Meerkat.Mix2Proof.FirstMessage result = new Meerkat.Mix2Proof.FirstMessage();
            result.Clause0 = this.clause0.getFirstMessage();
            result.Clause1 = this.clause1.getFirstMessage();

            return result;
        }

        private Meerkat.Mix2Proof.FinalMessage getFinalMessage()
        {
            Meerkat.Mix2Proof.FinalMessage result = new Meerkat.Mix2Proof.FinalMessage();
            result.C0 = new Meerkat.BigInteger();
            result.C0.Data = this.challenge.ToByteArray();

            result.Clause0 = this.clause0.getFinalMessage();
            result.Clause1 = this.clause1.getFinalMessage();

            return result;
        }


        public Meerkat.Mix2Proof getProtobufMessage(uint switchIndex, uint layer, uint out0Index, uint out1Index)
        {
            Meerkat.Mix2Proof result = new Meerkat.Mix2Proof();
            result.location = new Meerkat.Mix2Proof.Location();
            result.location.switchIdx = (int)switchIndex;
            result.location.Layer = (int)layer;
            result.location.Out0 = (int)out0Index;
            result.location.Out1 = (int)out1Index;

            result.firstMessage = getFirstMessage();
            result.finalMessage = getFinalMessage();
            return result;
        }

        public BigInteger challenge;
        public AndProof clause0;
        public AndProof clause1;

        private Meerkat.Mix2Proof.FirstMessage m_originalMessage;
    }

}
