﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Math;

namespace Mixnet.math.misc
{
    public class AdditiveZGroup : Group
    {
        /**
         *	Construct a Z/nZ additive group. 
         *  where order == n.
         */
        public AdditiveZGroup(BigInteger order)
        {
            m_order = order;
        }

        public BigInteger getOrder()
        {
            return m_order;
        }

        public GroupElement getZero()
        {
            return new AdditiveZElement(new BigInteger("0"), this); 
        }

        public AdditiveZElement createElement(BigInteger element)
        {
            return new AdditiveZElement(element, this);
        }

        public BigInteger m_order;
    }
}
