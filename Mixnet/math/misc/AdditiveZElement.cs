﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Math;

namespace Mixnet.math.misc
{
    public class AdditiveZElement : GroupElement
    {

        public AdditiveZElement(BigInteger value, AdditiveZGroup group)
        {
            m_value = value;
            m_group = group;

            // normalize
            m_value = m_value.Mod(group.getOrder());
        }


        public override GroupElement add(GroupElement elem)
        {
            return new AdditiveZElement(m_value.Add(((AdditiveZElement)elem).m_value),
                                        this.m_group);
        }

        public override bool equals(GroupElement elem)
        {
            if (!(elem is AdditiveZElement))
            {
                throw new InvalidOperationException("elem is not an AdditiveZElement");
            }

            return this.m_value.Equals(((AdditiveZElement)elem).m_value);
        }

        public override Group getGroup()
        {
            return m_group;
        }

        public override GroupElement multiply(BigInteger k)
        {
            return new AdditiveZElement(m_value.Multiply(k),
                                        this.m_group);
        }

        public override GroupElement negate()
        {
            return new AdditiveZElement(m_value.Negate(), this.m_group);
        }

        public override GroupElement subtract(GroupElement elem)
        {
            return new AdditiveZElement(m_value.Subtract(((AdditiveZElement)elem).m_value),
                                        this.m_group);
        }

        public override string toString()
        {
            return m_value.ToString() + " (mod " + m_group.getOrder().ToString() + ")";
        }

        public override byte[] encode()
        {
            return m_value.ToByteArray();
        }

        private BigInteger m_value;
        private AdditiveZGroup m_group;
    }
}
