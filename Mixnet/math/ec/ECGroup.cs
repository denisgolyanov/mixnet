﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;

namespace Mixnet.math.ec
{
    public class ECGroup : Group
    {
        public ECGroup(ECCurve curve)
        {
            m_curve = curve;
        }

        public BigInteger getOrder()
        {
            return m_curve.Order;
        }

        public GroupElement getZero()
        {
            return new ECElement(m_curve.Infinity, this);
        }

        public ECElement createPoint(BigInteger x, BigInteger y)
        {
            return new ECElement(m_curve.CreatePoint(x, y), this);
        }

        public ECElement createPoint(ECPoint point)
        {
            return new ECElement(point, this);
        }

        public ECCurve getCurve()
        {
            return m_curve;
        }

        private ECCurve m_curve; 
    }
}
