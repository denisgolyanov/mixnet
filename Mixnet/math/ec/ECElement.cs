﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;

namespace Mixnet.math.ec
{
    public class ECElement : GroupElement
    {
        public ECElement(ECPoint curvePoint, ECGroup group)
        {
            curvePoint =  curvePoint.Normalize();
            m_point = curvePoint;
            m_curveGroup = group;
        }

        public ECElement(byte[] asn1CompressedPoint, ECCurve curve)
            : this(curve.DecodePoint(asn1CompressedPoint), new ECGroup(curve))
        {
        }

        public override GroupElement add(GroupElement elem)
        {
            ECPoint result = m_point.Add(((ECElement)elem).m_point);
            return new ECElement(result, m_curveGroup);
        }

        public override bool equals(GroupElement elem)
        {
            if (!(elem is ECElement))
            {
                throw new InvalidOperationException("elem is not an ECElement");
            }

            return m_point.Equals(((ECElement)elem).m_point);
        }

        public override Group getGroup()
        {
            return m_curveGroup;
        }

        public override GroupElement multiply(BigInteger k)
        {
            ECPoint result = m_point.Multiply(k);
            return new ECElement(result, m_curveGroup);
        }

        public override GroupElement negate()
        {
            ECPoint result = m_point.Negate();
            return new ECElement(result, m_curveGroup);
        }

        public override GroupElement subtract(GroupElement elem)
        {
            ECPoint result = m_point.Subtract(((ECElement)elem).m_point);
            return new ECElement(result, m_curveGroup);
        }

        public override string toString()
        {
            return m_point.ToString();  
        }

        public override byte[] encode()
        {
            return m_point.GetEncoded(true);
        }

        public ECPoint getPoint()
        {
            return m_point;
        }

        private ECPoint m_point;
        private ECGroup m_curveGroup;
    }
}
