﻿using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.math
{
    public abstract class GroupElement
    {
        /**
         *	retrieves the associated group
         */
        public abstract Group getGroup();

        /**
         *	The operation of the group.
         */
        public abstract GroupElement add(GroupElement elem);

        /**
         *	@return the negation of current element
         */
        public abstract GroupElement negate();

        /**
         *	@return this - elem
         */
        public abstract GroupElement subtract(GroupElement elem);

        /**
         *	@return the multiplication result by multiplying current element by scalar k
         */
        public abstract GroupElement multiply(BigInteger k);

        /**
         *	@return true iff this == eleme
         */
        public abstract bool equals(GroupElement elem);

        /**
         *	@return a string representation of the element
         */
        public abstract String toString();

        /**
         *	@return the encoded form of the group element
         *          for EC, this must return an ASN.1 encoded point (compressed).
         */
        public abstract byte[] encode();

        public override bool Equals(object obj)
        {
            if (!(obj is GroupElement))
            {
                throw new InvalidOperationException("obj is not a group element");
            }

            return this.equals((GroupElement)obj);
        }
    }
}
