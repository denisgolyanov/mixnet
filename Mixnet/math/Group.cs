﻿using Org.BouncyCastle.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.math
{
    public interface Group
    {
        /**
         *	Retrieve the group order
         */
        BigInteger getOrder();

        /**
         *	Retrieve the 'Zero' element of the group.
         */
        GroupElement getZero();
    }
}
