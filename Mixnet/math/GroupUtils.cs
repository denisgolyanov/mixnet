﻿using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mixnet.math
{
    public class GroupUtils
    {
        /**
        *	Computes a random value k, s.t. 0 < k < group_order. 
        */
        public static BigInteger getRandomIntegerModulusGroupOrder(Group g, SecureRandom random)
        {
            int nBitLength = g.getOrder().BitCount;
            BigInteger k;
            do
            {
                k = new BigInteger(nBitLength, random);
            }
            while (k.Equals(0) || (k.CompareTo(g.getOrder()) >= 0));
            return k;
        }

        /**
         *	Creates a random element in the group, by using the given generator
         */
        public static GroupElement createRandomElement(GroupElement generator, SecureRandom random)
        {
            BigInteger randomK = getRandomIntegerModulusGroupOrder(generator.getGroup(), random);
            return generator.multiply(randomK);
        }
    }
}
