﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Org.BouncyCastle.Math;
using Mixnet.math;
using Org.BouncyCastle.Asn1;
using Mixnet.mix_network;
using Mixnet.crypto;

namespace Mixnet
{
    public class Program
    {

        const int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
        const string MODE_VERIFIER = "v";
        const string MODE_VERIFIER_STRICT = "vs";
        const string MODE_PROVER = "p";

        const string VERIFIER_SUCCESS_STRING = "OK";
        const string VERIFIFER_FAILURE_STRING = "BAD";

        static void printUsage()
        {
            Console.WriteLine("Invalid usage");
            Console.WriteLine("To invoke the prover, use: Mixnet p <key_file_path> <encrypted_inputs_path> <result_file_path>");
            Console.WriteLine("To invoke the verifier, use: Mixnet v <key_file_path> <prover_output_path> <result_file_path>");
            Console.WriteLine("To invoke the verifier in strict mode, use: Mixnet vs <key_file_path> <prover_output_path> <result_file_path>");
        }

        static void prove(ElGamalEncryptionParameters encryptParams, string encryptedInputsFilePath, string outputFilePath)
        {
            try
            {
                Console.WriteLine("Proving for inputs at {0}, output will be written to {1}", encryptedInputsFilePath, outputFilePath);
                PermutationNetwork network = MixnetCreator.createMixnet(encryptParams, encryptedInputsFilePath);
                MixnetSerializer.serializeNetworkToFile(outputFilePath, network);
            }
            catch (Exception e)
            {
                Console.WriteLine("Prover failed with exception {0}", e.Message);
            }
        }

        static void verify(ElGamalEncryptionParameters encryptParams, string proverOutputFilePath, bool strict, string outputFilePath)
        {
            bool success = true;

            try
            {
                Console.WriteLine("Verifying prover output at {0}, strict mode: {1}, output will be written to {2}",
                    proverOutputFilePath, strict, outputFilePath);

                PermutationNetwork mixnet = MixnetSerializer.parseMixnet(encryptParams, proverOutputFilePath);
                MixnetVerifier.verifyMixnet(encryptParams, mixnet, strict);

                Console.WriteLine("Verification succeeded!");
            } 
            catch (Exception e)
            {
                Console.WriteLine("Verification failed: {0}", e.Message);
                success = false;
            }

            if (success)
            {
                System.IO.File.WriteAllText(outputFilePath, VERIFIER_SUCCESS_STRING);
            } 
            else
            {
                System.IO.File.WriteAllText(outputFilePath, VERIFIFER_FAILURE_STRING);
            }
        }

        //
        // Two possible usages:
        // Mixnet v key_file_path prover_output_path    result_file_path
        // Mixnet p key_file_path encrypted_inputs_path result_file_path
        //
        static void Main(string[] args)
        {

            if (args.Length != EXPECTED_NUMBER_OF_ARGUMENTS)
            {
                printUsage();

                return;
            }

            string mode = args[0];
            if (mode != MODE_VERIFIER && mode != MODE_PROVER && mode != MODE_VERIFIER_STRICT)
            {
                printUsage();

                return;
            }

            string keyFilePath = args[1];
            string inputFilePath = args[2];
            string resultsFilePath = args[3];

            ElGamalEncryptionParameters encryptParams;

            try
            {
                 encryptParams = MixnetSerializer.loadKeyInformation(keyFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to read key file at {0}, exception: {1}", keyFilePath, e.Message);
                return;
            }

            if (mode == MODE_PROVER)
            {
                prove(encryptParams, inputFilePath, resultsFilePath);
            }
            else
            {
                bool strict = mode == MODE_VERIFIER_STRICT;
                verify(encryptParams, inputFilePath, strict, resultsFilePath);
            }
        }
       
    }
}
