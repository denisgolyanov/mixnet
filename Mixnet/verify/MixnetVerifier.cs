﻿using Mixnet.crypto;
using System;


namespace Mixnet.mix_network
{

    public class DuplicateInputMappingException : Exception
    {
        public DuplicateInputMappingException(int layer) : 
            base(String.Format("More than one output at layer {0} is mapped to the same input at the consecutive layer", layer))
        {
        }
    }

    public class InputIndexOutOfBoundsException : Exception
    {
        public InputIndexOutOfBoundsException(int layer, int outputIndex, int inputIndex) :
            base(String.Format("Output at layer {0}, index {1} is mapped to an input index that is out of bounds", layer, outputIndex, inputIndex))
        {
        }
    }

    public class IncorrectOutputMappingException : Exception
    {
        public IncorrectOutputMappingException(int layer, int outputIndex, int actualInputIndex, int correctInputIndex) :
            base(String.Format("Output {0} at layer {1}, is mapped to input {2} instead of {3} according to benes", 
                               outputIndex, layer, actualInputIndex, correctInputIndex))
        {
        }
    }

    public class IncorrectNumberOfLayersException : Exception
    {
        public IncorrectNumberOfLayersException(int actualLayers, int expectedLayers) :
            base(String.Format("Network has {0} layers, instead of excepted {1} layers according to benes construction",
                               actualLayers, expectedLayers))
        {
        }
    }

    public class IncorrectSwitchProof : Exception
    {
        public IncorrectSwitchProof(int layerIndex, int switchIndex) :
            base(String.Format("The switch proof for switch at index {0} at layer {1} is incorrect!", switchIndex, layerIndex))
        {
        }
    }

    public class MixnetVerifier
    {
        /**
         *	Verifies the correctness of the physical structure of the given permutation network
         *
         *  This includes the following checks:
         *        1. The permutation network represents a valid permutation - 
         *           No more than one output is mapped to the same input in the next layer
         *           and no output is mapped to an input index that is out of bounds.
         *        2. [strict only] the number of layers is as described in the benes construction
         *        3. [strict only] the output to input mapping is as described in the benes construction
         *
         *  If any check fails, an appropriate exception is thrown to indicate failure. 
         * 
         *  @param permNetwork the network to verify
         *  @param strictMode whether to perform strict verifications, as explained above. 
         */

        private static void verifyPhysicalStructure(PermutationNetwork permNetwork, bool strictMode)
        {

            Console.WriteLine("Verifier: verifying physical properties");

            // check that no two outputs are mapped to the same output in the next layer
            //

            Console.WriteLine("Verifier: Checking if two outputs are mapped to the same input");
            for (int layer = 0; layer < permNetwork.getNumLayers(); layer++)
            {
                bool[] checkDuplication = new bool[permNetwork.getNumCipherTextsInLayer()];
                for (int cipherIndex = 0; cipherIndex < permNetwork.getNumCipherTextsInLayer(); cipherIndex++)
                {
                    if (permNetwork.getMapTable()[layer, cipherIndex] < 0 
                    ||  permNetwork.getMapTable()[layer, cipherIndex] >= permNetwork.getNumCipherTextsInLayer())
                    {
                        Console.WriteLine("Verifier: an out of bounds output to input index mapping has been detected at layer {0}", layer);
                        throw new InputIndexOutOfBoundsException(layer, cipherIndex, permNetwork.getMapTable()[layer, cipherIndex]);
                    }

                    if (!checkDuplication[permNetwork.getMapTable()[layer, cipherIndex]])
                    {
                        checkDuplication[permNetwork.getMapTable()[layer, cipherIndex]] = true;
                    }
                    else
                    {
                        Console.WriteLine("Verifier: more than one output is mapped to the same input at layer {0}", layer);
                        throw new DuplicateInputMappingException(layer);
                    }
                }
            }

            // verify that the network structure matches the one specified in the benes algorithm
            // 
            if (strictMode)
            {
                Console.WriteLine("Verifier [strict]: verifying number of layers");
                BenesNetwork benes = new BenesNetwork(permNetwork.getNumCipherTextsInLayer());
                if (permNetwork.getNumLayers() != benes.getLayers())
                {
                    Console.WriteLine("Verifier: number of layers ({0}) is not as expected ({1})", permNetwork.getNumLayers(), benes.getLayers());
                    throw new IncorrectNumberOfLayersException(permNetwork.getNumLayers(), benes.getLayers());
                }

                Console.WriteLine("Verifier [strict]: verifying network is constructed correctly, according to benes");
                for (int layer = 0; layer < permNetwork.getNumLayers(); layer++)
                    for (int outputIndex = 0; outputIndex < permNetwork.getNumCipherTextsInLayer(); outputIndex++)
                    {
                        int actualInputMapIndex = permNetwork.getMapTable()[layer, outputIndex];
                        int expectedInputMapIndex = benes.getNextIndex((int)Math.Log(permNetwork.getNumCipherTextsInLayer(), 2), layer+1, outputIndex);
                        if (actualInputMapIndex != expectedInputMapIndex)
                        {
                            Console.WriteLine("Verifier: input {0} in layer {1} isn't suitable for benes network (mapped to {2} instead of {3})",
                                              outputIndex, layer, actualInputMapIndex, expectedInputMapIndex);
                            throw new IncorrectOutputMappingException(layer, outputIndex, actualInputMapIndex, expectedInputMapIndex);
                        }
                    }
            }
        }


        /**
         *	Verifies the correctness of the physical structure of the network, as well
         *  as the correctness of all the switch proofs.
         *  
         *  @see verifyPhysicalStructure for a list of physical verifications
         * 
         *  @param strict whether to perform strict mode verifications
         *  
         *  @throws on any verification failure, an appropriate exception is thrown.
         */

        public static void verifyMixnet(ElGamalEncryptionParameters encryptParams, PermutationNetwork mixnet, bool strict)
        {
            verifyPhysicalStructure(mixnet, strict);

            Console.WriteLine("Verifier: verifying switch gate proofs ...");
            bool[,] results = mixnet.verifySwitchGateProofs();
            bool valid = true;
            int invalidSwitchIndex = 0;
            int invalidLayerIndex = 0;

            Console.WriteLine("Verifier: Proof correctness table");
            for (int switchIndex = 0; switchIndex < results.GetLength(1); switchIndex++)
            {
                for (int layerIndex = 0; layerIndex < results.GetLength(0); layerIndex++)
                {
                    Console.Write("{0} ", results[layerIndex, switchIndex]);

                    if (!results[layerIndex, switchIndex])
                    {
                        valid = false;
                        invalidSwitchIndex = switchIndex;
                        invalidLayerIndex = layerIndex;
                    }
                }
                Console.WriteLine();
            }

            if (!valid)
            {
                throw new IncorrectSwitchProof(invalidLayerIndex, invalidSwitchIndex);
            }

        }

    }
}
