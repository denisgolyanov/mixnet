﻿using Mixnet.crypto;
using Mixnet.math;
using Org.BouncyCastle.Math;

namespace Mixnet.proofs
{
    /**
     *	Houses the logic responsible for verifying a single switch operation ZK proof. 
     *  The verifier verifies the correctness of the disjunction, by verifying the correctness
     *  of the transcripts of both its clauses (The challenge for the first clause is given inside the proof object
     *  while the challenge for the second clause is computed using the oracle challenge and given challenge). 
     *
     *  a conjunction is verified by using the same challenge for both clauses. Finally, a DLOG equivalence is verified
     *  using Schnorr's protocol. 
     */
    public class Verifier
    {
        public Verifier(ElGamalEncryptionParameters encryptParams)
        {
            m_eParams = encryptParams;
        }

        public bool verify(ElGamalPair im1, ElGamalPair im2, ElGamalPair om1, ElGamalPair om2, SwitchProof proof)
        {
            // prepare statements
            Mix2Statements statements = new Mix2Statements(im1, im2, om1, om2);

            // prepare challenges
            BigInteger hashedChallenge = FiatShamir.computeOracleChallenge(proof);
            BigInteger ch1 = hashedChallenge.Subtract(proof.challenge).Mod(m_eParams.Group.getOrder());

            // compute proofs
            return verifyAndProof(proof.clause0, statements.clause0, proof.challenge)
                && verifyAndProof(proof.clause1, statements.clause1, ch1);
        }

        private bool verifyAndProof(AndProof proof, AndProofStatements statements, BigInteger challenge)
        {
            bool result = false;
            result = verifyDlogProof(proof.clause0.firstmsg.gr,
                                     proof.clause0.firstmsg.hr,
                                     statements.clause0.powerOfG,
                                     statements.clause0.powerOfH,
                                     proof.clause0.finalmsg.xcr,
                                     challenge);
            result &= verifyDlogProof(proof.clause1.firstmsg.gr,
                                      proof.clause1.firstmsg.hr,
                                      statements.clause1.powerOfG,
                                      statements.clause1.powerOfH,
                                      proof.clause1.finalmsg.xcr,
                                      challenge);
            return result;
        }

        private bool verifyDlogProof(GroupElement gr, GroupElement hr, GroupElement a, GroupElement b, BigInteger xcr, BigInteger c)
        {
            GroupElement g = m_eParams.Generator;
            GroupElement h = m_eParams.PublicKey;

            return gr.add(a.multiply(c)).Equals(g.multiply(xcr)) 
                && hr.add(b.multiply(c)).Equals(h.multiply(xcr));
        }

        private ElGamalEncryptionParameters m_eParams;
    }
}
