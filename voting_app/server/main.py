import tornado.ioloop
import tornado.web

from BaseHandler import BaseHandler
from StatusManager import StatusManager

DEFAULT_PORT_HTTP = 8080


"""
This handler is responsible for providng the voters page
"""
class VoterHandler(BaseHandler):
    def get(self):
        with open(r'../client/voter.html', 'r') as f:
            data = f.read()
        self.write(data)
        self.finish()

"""
This handler is responsible for providing the admin page
"""
class AdminHandler(BaseHandler):
    def get(self):
        with open(r'../client/admin.html', 'r') as f:
            data = f.read()
        self.write(data)
        self.finish()

def main():
    apps = [
        (r'/admin', AdminHandler),
        (r'/', VoterHandler),
        (r'/status', StatusManager),
        (r"/images/(.*)", tornado.web.StaticFileHandler, {"path": "./../client"}),
        (r"/scripts/(.*)", tornado.web.StaticFileHandler, {"path": "./../client/scripts"}),
        (r"/results/(.*)", tornado.web.StaticFileHandler, {"path": "./binaries"})
    ]
    server = tornado.web.Application(apps)
    server.listen(DEFAULT_PORT_HTTP)
    tornado.ioloop.IOLoop.instance().start()
    return


if '__main__' == __name__:
    main()