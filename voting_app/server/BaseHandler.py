import tornado.web

class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, *args, **kwargs):

        super(BaseHandler, self).__init__(*args, **kwargs)

        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
