import tornado.web
from mixnet_wrapper import issue_computation, has_computation_completed


"""
    Manages the state machine of the server.
    To fetch current state and additional information regarding the state, a get request is to be sent.
    Administartors may change the server state by posting a command.
"""


class StatusManager(tornado.web.RequestHandler):

    STATE_INIT = "init"  # at this stage the administrator sets up the candidate names

    STATE_VOTE = "vote"  # candidates were selected, now voters may now vote
                         # till the admin stops the poll

    STATE_CALCULATING = "calculating" # the admin has ended the poll and the mix net is now being computed.
                                      # this stage also includes an execution of the verifier over the results.

    STATE_DONE = "done"               # results are ready. both the votes and the administrator may view the results.
                                      # admin may choose to reset the state back to STATE_INIT.

    state = STATE_INIT

    # a list of names
    participants = []

    # a list of names, each name is a vote for said candidate.
    votes = []

    # an object of type ComputationResult, representing the computation results
    # this includes the prover and the verifications
    result = []

    # administrator commands
    COMMAND_SUBMIT_PARTICIPANTS = "participants"
    COMMAND_COMPUTE_RESULT = "computeResults"
    COMMAND_RESTART_POLL = "restartPoll"
    COMMAND_CAST_VOTE = "castVote"

    def __init__(self, *args, **kwargs):
        super(StatusManager, self).__init__(*args, **kwargs)

        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def get(self):
        additional_data = ""
        if self.state == self.STATE_VOTE:
            additional_data = {"participants": self.participants,
                               "numVotes": len(self.votes)}

        if self.state == self.STATE_CALCULATING:
            if has_computation_completed():
                StatusManager.result = ComputationResult()
                StatusManager.state = self.STATE_DONE

        if self.state == self.STATE_DONE:
            additional_data = StatusManager.result.to_done_additional_information()

        self.write(tornado.escape.json_encode({self.state: additional_data}))
        self.finish()

    def options(self, *args, **kwargs):
        # This is required for debugging purposes.
        pass

    def post(self):
        command_dict = tornado.escape.json_decode(self.request.body)
        if StatusManager.COMMAND_SUBMIT_PARTICIPANTS in command_dict:
            StatusManager.participants = tornado.escape.json_decode(command_dict[StatusManager.COMMAND_SUBMIT_PARTICIPANTS])

            # clients are now ready to vote! WOOH
            StatusManager.state = self.STATE_VOTE

        elif StatusManager.COMMAND_COMPUTE_RESULT in command_dict:
            StatusManager.state = self.STATE_CALCULATING
            issue_computation(StatusManager.votes)

        elif StatusManager.COMMAND_RESTART_POLL in command_dict:
            StatusManager.participants = []
            StatusManager.votes = []
            StatusManager.results = []
            StatusManager.state = self.STATE_INIT

        elif StatusManager.COMMAND_CAST_VOTE in command_dict and StatusManager.state == StatusManager.STATE_VOTE:
            candidate_name = command_dict[StatusManager.COMMAND_CAST_VOTE]
            StatusManager.votes.append(candidate_name.lower())


class ComputationResult(object):
    def __init__(self):
        from mixnet_wrapper import prover_failure_message, \
            our_verifier_failure_message, jar_verifier_failure_message, output_votes

        self._prover_failure_message = None
        self._our_verifier_failure_message = None
        self._jar_verifier_failure_message = None
        self._candidate_to_vote_count = None

        if prover_failure_message is not None:
            self._prover_failure_message = prover_failure_message
            return

        if our_verifier_failure_message is not None:
            self._our_verifier_failure_message = our_verifier_failure_message

        if jar_verifier_failure_message is not None:
            self._jar_verifier_failure_message = jar_verifier_failure_message

        self._candidate_to_vote_count = {}
        for participant in StatusManager.participants:
            self._candidate_to_vote_count[participant.lower()] = 0

        for vote in output_votes:
            self._candidate_to_vote_count[vote] += 1

    def to_done_additional_information(self):
        if self._prover_failure_message is not None:
            return {'prover_failure': self._prover_failure_message}

        additional_data = {}
        if self._our_verifier_failure_message is not None:
            additional_data['our_verifier_failure'] = self._our_verifier_failure_message
        if self._jar_verifier_failure_message is not None:
            additional_data['jar_verifier_failure'] = self._jar_verifier_failure_message

        additional_data['results'] = [{'name': candidate, 'count': self._candidate_to_vote_count[candidate]}
                                      for candidate in self._candidate_to_vote_count.keys()]

        return additional_data
