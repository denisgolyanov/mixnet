from threading import Thread
from subprocess import PIPE, Popen
import os

BINARIES_DIR = 'binaries'
KEY_PATH = os.path.join(BINARIES_DIR, 'ecelgamal.key')
JAR_MIXER_PATH = os.path.join(BINARIES_DIR, 'mixer.jar')
ENCRYPTED_INPUTS_PATH = os.path.join(BINARIES_DIR, 'inputs.enc')
PROVER_OUTPUT_PATH = os.path.join(BINARIES_DIR, 'proof')
OUR_MIXER_PATH = os.path.join(BINARIES_DIR, 'Mixnet.exe')
RAW_INPUTS_PATH = os.path.join(BINARIES_DIR, 'inputs.txt')
VERIFIER_RESULTS_PATH = os.path.join(BINARIES_DIR, 'verifier_output.txt')
DECRYPTED_RESULTS_PATH = os.path.join(BINARIES_DIR, 'decrypted_results.txt')

input_votes = None
prover_failure_message = None
our_verifier_failure_message = None
jar_verifier_failure_message = None
output_votes = None


def _encrypt_inputs():
    p = Popen('java -jar {mixer_jar} -i {inputs_path} -k {key_file_path} -e -o {encrypted_inputs_path}'.format(
        mixer_jar=JAR_MIXER_PATH,
        inputs_path=RAW_INPUTS_PATH,
        key_file_path=KEY_PATH,
        encrypted_inputs_path=ENCRYPTED_INPUTS_PATH
    ), shell=True, stdout=PIPE, stderr=PIPE)

    stdout, stderr = p.communicate()

    if not os.path.exists(ENCRYPTED_INPUTS_PATH):
        raise Exception(stderr)


def _mix_and_prove():
    result = os.popen('{mixer_exe} p {key_path} {enc_inputs_path} {output_path}'.format(
        mixer_exe=OUR_MIXER_PATH,
        key_path=KEY_PATH,
        enc_inputs_path=ENCRYPTED_INPUTS_PATH,
        output_path=PROVER_OUTPUT_PATH
    )).read()

    if not os.path.exists(PROVER_OUTPUT_PATH):
        raise Exception(result.splitlines()[-1])


def _verify_with_mixnet_exe():
    # verify [strict] using our verifier
    result = os.popen('{mixer_exe} vs {key_path} {proof_path} {output_path}'.format(
        mixer_exe=OUR_MIXER_PATH,
        key_path=KEY_PATH,
        proof_path=PROVER_OUTPUT_PATH,
        output_path=VERIFIER_RESULTS_PATH
    )).read()

    if not os.path.exists(VERIFIER_RESULTS_PATH) or open(VERIFIER_RESULTS_PATH, 'r').read() != 'OK':
        raise Exception(result.splitlines()[-1])


def _verify_with_jar():
    p = Popen('java -jar {mixer_jar} -i {inputs_path} -k {key_file_path}'.format(
        mixer_jar=JAR_MIXER_PATH,
        inputs_path=PROVER_OUTPUT_PATH,
        key_file_path=KEY_PATH,
        encrypted_inputs_path=ENCRYPTED_INPUTS_PATH
    ), shell=True, stdout=PIPE, stderr=PIPE)

    stdout, stderr = p.communicate()

    if 'Verification successful' not in stdout:
        raise Exception(stdout)


def _decrypt_results():
    global output_votes

    p = Popen('java -jar {mixer_jar} -i {inputs_path} -k {key_file_path} -d -o {decrypted_result}'.format(
        mixer_jar=JAR_MIXER_PATH,
        inputs_path=PROVER_OUTPUT_PATH,
        key_file_path=KEY_PATH,
        encrypted_inputs_path=ENCRYPTED_INPUTS_PATH,
        decrypted_result=DECRYPTED_RESULTS_PATH
    ), shell=True, stdout=PIPE, stderr=PIPE)

    stdout, stderr = p.communicate()

    if not os.path.exists(DECRYPTED_RESULTS_PATH):
        raise Exception(stderr)

    with open(DECRYPTED_RESULTS_PATH, 'r') as r:
        for line in r.readlines():
            if len(line.strip()) > 0:
                output_votes += [line.strip().lower()]


def _create_raw_inputs_file():
    with open(RAW_INPUTS_PATH, 'w') as f:
        for vote in input_votes:
            f.write(vote + os.linesep)


def _silent_delete(file_path):
    try:
        os.remove(file_path)
    except:
        pass


def compute_results_func():
    global prover_failure_message
    global our_verifier_failure_message
    global jar_verifier_failure_message
    global output_votes

    prover_failure_message = None
    our_verifier_failure_message = None
    jar_verifier_failure_message = None
    output_votes = []

    _silent_delete(VERIFIER_RESULTS_PATH)
    _silent_delete(RAW_INPUTS_PATH)
    _silent_delete(ENCRYPTED_INPUTS_PATH)
    _silent_delete(PROVER_OUTPUT_PATH)
    _silent_delete(DECRYPTED_RESULTS_PATH)

    _create_raw_inputs_file()

    try:
        _encrypt_inputs()
        _mix_and_prove()
        _decrypt_results()
    except Exception as e:
        prover_failure_message = e.message

        # if prover fails we don't attempt to verify the result.
        return

    try:
        _verify_with_mixnet_exe()
    except Exception as e:
        our_verifier_failure_message = e.message

    try:
        _verify_with_jar()
    except Exception as e:
        jar_verifier_failure_message = e.message


MIXER_THREAD = None


def issue_computation(votes):
    global MIXER_THREAD

    if MIXER_THREAD is not None and MIXER_THREAD.isAlive():
        # thread is already mixing votes
        return

    global input_votes
    input_votes = votes

    MIXER_THREAD = Thread(target=compute_results_func)
    MIXER_THREAD.start()


def has_computation_completed():
    global MIXER_THREAD
    return not MIXER_THREAD.isAlive()