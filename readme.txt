README.txt

Requirements:

Linux Machine
java
Python 2.7 : apt-get install python
	Tornado lib : pip install tornado
	pip lib
Mono .Net for Linux : apt-get install mono-complete


Install Instructions:
	simply run install.sh for auto install or install it manually by following the following instructions:

1. Get the source code
2. Compile (after installing mono)
	give run permission to mixnet/tools/protogen/protogen.exe : chmod 775 potogen.exe
	run "xbuild mixnet/Mixnet/Mixnet.csproj" to compile the project
3. Copy file
	copy the binary to vote app folder : cp -f mixnet/Mixnet/bin/Debug/Mixnet.exe mixnet/voting_app/server/binaries/Mixnet.exe)

4. Run the server (need to be run from mixnet/voting_app/server/)
	python main.py
5. Admin panel: http://HOST:8080/admin"
6. Voting panel:  http://HOST:8080/"