﻿using System;
using System.Collections;

using NUnit.Framework;

using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Mixnet.math;
using Mixnet.math.ec;
using Mixnet.math.misc;
using Mixnet.crypto;
using Org.BouncyCastle.Utilities.Encoders;
using UnitTests.common;

namespace UnitTests.crypto
{
    [TestFixture]
    public class ElGamalTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();

        public void ElGamalEncryptionTestImpl(GroupElement elem,
                                              ElGamalEncryptionParameters encryptParams,
                                              BigInteger privateKey)
        {
            ElGamalPair pair = ElGamal.encrypt(elem, encryptParams);
            BigInteger k;
            for (int i = 0; i < 1000; ++i)
            {
                pair = ElGamal.reEncrypt(pair, encryptParams, out k);
            }

            Utils.AssertElementsEqual("Encryption or decryption has failed", ElGamal.decrypt(pair, privateKey), elem);
        }

        [Test]
        public void TestElGamalEncryptionZnZ()
        {
            BigInteger privateKey;
            ElGamalEncryptionParameters encryptParams = Utils.createZnZCurveParameters(this.Random, out privateKey);

            for (int i = 0; i < 100; ++i)
            {
                GroupElement elem = Utils.createRandomElement(encryptParams.Generator, this.Random);
                ElGamalEncryptionTestImpl(elem, encryptParams, privateKey);
            }
        }

        [Test]
        public void TestElGamalEncryptionFpCurve()
        {
            BigInteger privateKey = null;
            ElGamalEncryptionParameters encryptParams = Utils.createFpCurveParameters(this.Random, out privateKey);

            for (int i = 0; i < 3; ++i)
            {
                GroupElement elem = Utils.createRandomElement(encryptParams.Generator, this.Random);
                ElGamalEncryptionTestImpl(elem, encryptParams, privateKey);
            }
        }

        [Test]
        public void TestElGamalEncryptionSecp256r1Curve()
        {
            BigInteger privateKey = null;
            ElGamalEncryptionParameters encryptParams = Utils.createSecp256r1CurveParameters(this.Random, out privateKey);

            for (int i = 0; i < 3; ++i)
            {
                GroupElement elem = Utils.createRandomElement(encryptParams.Generator, this.Random);
                ElGamalEncryptionTestImpl(elem, encryptParams, privateKey);
            }
        }

        [Test]
        public void TestElGamalPairSerialization()
        {
            BigInteger privateKey;
            ElGamalEncryptionParameters encryptParams = Utils.createSecp256r1CurveParameters(this.Random, out privateKey);
            GroupElement element1 = Utils.createRandomElement(encryptParams.Generator, this.Random);
            GroupElement element2 = Utils.createRandomElement(encryptParams.Generator, this.Random);

            ElGamalPair pair = new ElGamalPair(element1, element2);
            Meerkat.RerandomizableEncryptedMessage rerandomizedMessage = pair.getPairMessage();
            ElGamalPair deserializedPair = new ElGamalPair(rerandomizedMessage, ((ECGroup)encryptParams.Group).getCurve());

            Assert.True(pair.G.Equals(deserializedPair.G), "G element is different");
            Assert.True(pair.M.Equals(deserializedPair.M), "M element is different");
        }

    }
}
