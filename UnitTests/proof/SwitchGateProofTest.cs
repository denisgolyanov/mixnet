﻿using System;
using System.Collections;

using NUnit.Framework;

using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Mixnet.math;
using Mixnet.math.ec;
using Mixnet.math.misc;
using Mixnet.crypto;
using Org.BouncyCastle.Utilities.Encoders;
using UnitTests.common;
using Mixnet.mix_network;
using Mixnet.proofs;

namespace UnitTests.mix_network
{
    [TestFixture]
    public class SwitchGateProofTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();

        public void TestSwitchGateProofImpl(BigInteger privatekey, ElGamalEncryptionParameters encryptParams)
        {
            Prover prov = new Prover(encryptParams);
            Verifier verif = new Verifier(encryptParams);
            
            ElGamalPair in0 = new ElGamalPair(encryptParams.Group.getZero(),
                                              Utils.createRandomElement(encryptParams.Generator, this.Random));

            ElGamalPair in1 = new ElGamalPair(encryptParams.Group.getZero(),
                                              Utils.createRandomElement(encryptParams.Generator, this.Random));


            BigInteger dlog0, dlog1;
            ElGamalPair out1 = ElGamal.reEncrypt(in0, encryptParams, out dlog0);
            ElGamalPair out2 = ElGamal.reEncrypt(in1, encryptParams, out dlog1);

            bool[] mixValues = new bool []{ true, false };
            for (int i = 0; i < mixValues.Length; ++i)
            {
                Prover switchProver = new Prover(encryptParams);
                SwitchProof proof = switchProver.proveSwitch(in0, in1,
                                                             out1, out2, dlog0, dlog1,
                                                             mixValues[i]);

                bool res = false;

                if (mixValues[i])
                {
                    res = verif.verify(in0, in1, out2, out1, proof);
                } else
                {
                    res = verif.verify(in0, in1, out1, out2, proof);
                }

                Assert.True(res, "verification failed");
            }
        }

        [Test]
        public void TestSwitchGateProof()
        {
            BigInteger privateKey;
            ElGamalEncryptionParameters encryptParams = Utils.createZnZCurveParameters(this.Random, out privateKey);

            TestSwitchGateProofImpl(privateKey, encryptParams);

            encryptParams = Utils.createFpCurveParameters(this.Random, out privateKey);

            TestSwitchGateProofImpl(privateKey, encryptParams);

            encryptParams = Utils.createSecp256r1CurveParameters(this.Random, out privateKey);

            TestSwitchGateProofImpl(privateKey, encryptParams);
        }


        [Test]
        public void TestProofSerialization()
        {
            BigInteger privateKey;
            ElGamalEncryptionParameters encryptParams = Utils.createSecp256r1CurveParameters(this.Random, out privateKey);

            ElGamalPair in0 = new ElGamalPair(encryptParams.Group.getZero(),
                                              Utils.createRandomElement(encryptParams.Generator, this.Random));

            ElGamalPair in1 = new ElGamalPair(encryptParams.Group.getZero(),
                                              Utils.createRandomElement(encryptParams.Generator, this.Random));

            BigInteger dlog0, dlog1;
            ElGamalPair out1 = ElGamal.reEncrypt(in0, encryptParams, out dlog0);
            ElGamalPair out2 = ElGamal.reEncrypt(in1, encryptParams, out dlog1);

            Prover switchProver = new Prover(encryptParams);
            SwitchProof proof = switchProver.proveSwitch(in0, in1,
                                                         out1, out2, dlog0, dlog1,
                                                         false);
            Verifier verif = new Verifier(encryptParams);
            bool res = verif.verify(in0, in1, out1, out2, proof);

            Assert.True(res, "original verification failed");

            uint switchIndex = 5;
            uint layer = 3;
            uint out0Index = 9;
            uint out1Index = 22;

            Meerkat.Mix2Proof serializedProof = proof.getProtobufMessage(switchIndex, layer, out0Index, out1Index);
            Assert.True(switchIndex == serializedProof.location.switchIdx);
            Assert.True(layer == serializedProof.location.Layer);
            Assert.True(out0Index == serializedProof.location.Out0);
            Assert.True(out1Index == serializedProof.location.Out1);

            SwitchProof deserializedProof = new SwitchProof(serializedProof, ((ECGroup)encryptParams.Group).getCurve());

            Assert.True(proof.challenge.Equals(deserializedProof.challenge), "challenge does not match");

            verif = new Verifier(encryptParams);
            res = verif.verify(in0, in1, out1, out2, deserializedProof);
            Assert.True(res, "deserialized verification failed");

        }

    }
}
