﻿using System;
using System.Collections;

using NUnit.Framework;

using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Mixnet.math;
using Mixnet.math.ec;
using Mixnet.math.misc;
using Mixnet.crypto;
using Org.BouncyCastle.Utilities.Encoders;
using UnitTests.common;
using Mixnet.mix_network;
using Mixnet.proofs;

namespace UnitTests.mix_network
{
    [TestFixture]
    public class SwitchGateTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();


        [Test]
        public void TestSwitchGateOperation()
        {
            BigInteger privateKey;
            ElGamalEncryptionParameters encryptParams = Utils.createZnZCurveParameters(this.Random, out privateKey);

            TestSwitchGateImpl(privateKey, encryptParams);

            encryptParams = Utils.createFpCurveParameters(this.Random, out privateKey);

            TestSwitchGateImpl(privateKey, encryptParams);

            encryptParams = Utils.createSecp256r1CurveParameters(this.Random, out privateKey);

            TestSwitchGateImpl(privateKey, encryptParams);
        }

        public void TestSwitchGateImpl(BigInteger privateKey, ElGamalEncryptionParameters encryptParams)
        {
            // <1, M>, input to first column of gates
            ElGamalPair inputOne = new ElGamalPair(encryptParams.Group.getZero(),
                                                   Utils.createRandomElement(encryptParams.Generator, this.Random));

            ElGamalPair inputTwo = new ElGamalPair(encryptParams.Group.getZero(),
                                                   Utils.createRandomElement(encryptParams.Generator, this.Random));

            ElGamalPair originalInputOne = inputOne;
            ElGamalPair originalInputTwo = inputTwo;

            for (int i = 0; i < 100; ++i)
            {
                SwitchingGate sg = new SwitchingGate(encryptParams);

                sg.InputOne = inputOne;
                sg.InputTwo = inputTwo;

                bool cross = i % 2 == 0;

                SwitchProof proof = sg.doOperation(cross);

                Verifier verif = new Verifier(encryptParams);
                Assert.True(verif.verify(inputOne, inputTwo, sg.OutputOne, sg.OutputTwo, proof), "Incorrect proof for switch");

                inputOne = sg.OutputOne;
                inputTwo = sg.OutputTwo;
            }

            // Note that number of crosses is even
            Utils.AssertElementsEqual("inputOne does not match decrypted outputOne",
                                      originalInputOne.M,
                                      ElGamal.decrypt(inputOne, privateKey));

            Utils.AssertElementsEqual("inputTwo does not match decrypted outputTwo",
                                      originalInputTwo.M,
                                      ElGamal.decrypt(inputTwo, privateKey));
        }
    }
}
