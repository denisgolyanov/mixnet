﻿using System;

using NUnit.Framework;

using Org.BouncyCastle.Security;
using Mixnet.math;
using Mixnet.crypto;
using Mixnet.mix_network;
using Mixnet.proofs;
using System.IO;

namespace UnitTests.mix_network
{
    [TestFixture]
    public class VerifierTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();


        [Test]
        public void TestJarProofVerification()
        {
            String keyInformationFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\ecelgamal.key");
            String proofInputFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\large_mixed.proof");

            ElGamalEncryptionParameters encryptParams = MixnetSerializer.loadKeyInformation(keyInformationFilePath);

            PermutationNetwork network = MixnetSerializer.parseMixnet(encryptParams, proofInputFilePath);
            Assert.DoesNotThrow(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, true);
                }, "verification failed");

            // mess up a switch proof and make sure the verifier catches this.
            network.getProofsTable()[1, 2] = network.getProofsTable()[0, 0];

            Assert.Throws<Mixnet.mix_network.IncorrectSwitchProof>(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, true);
                }, "verifier failed to catch incorrect proof");
        }

        [Test]
        public void TestMixnetSerialization()
        {
            String keyInformationFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\ecelgamal.key");
            String proofInputFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\large_mixed.proof");
            String serializedNetworkPath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\serialized.proof");

            ElGamalEncryptionParameters encryptParams = MixnetSerializer.loadKeyInformation(keyInformationFilePath);

            PermutationNetwork network = MixnetSerializer.parseMixnet(encryptParams, proofInputFilePath);
            MixnetSerializer.serializeNetworkToFile(serializedNetworkPath, network);

            byte[] originalBytes = System.IO.File.ReadAllBytes(proofInputFilePath);
            byte[] ourBytes = System.IO.File.ReadAllBytes(serializedNetworkPath);

            Assert.IsTrue(originalBytes.Length == ourBytes.Length, "proof files are of different sizes");

            for (int i = 0; i < originalBytes.Length; ++i)
            {
                Assert.IsTrue(originalBytes[i] == ourBytes[i], "bytes differ");
            }
        }

        [Test]
        public void TestStrictVerification()
        {
            String keyInformationFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\ecelgamal.key");
            String serializedNetworkPath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\serialized.proof");

            ElGamalEncryptionParameters encryptParams = MixnetSerializer.loadKeyInformation(keyInformationFilePath);

            // The number of layers is correct here(according to Benet's network construction).
            PermutationNetwork network = createDirectNetwork(serializedNetworkPath, 3, 4, encryptParams);

            Assert.DoesNotThrow(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, false /* non strict */);
                }, "non strict verification failed");

            Assert.Throws<Mixnet.mix_network.IncorrectOutputMappingException>(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, true /* strict */);
                }, "strict verification should have failed");

            // Incorrect number of layers
            network = createDirectNetwork(serializedNetworkPath, 8, 64, encryptParams);

            Assert.DoesNotThrow(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, false /* non strict */);
                }, "non strict verification failed");

            Assert.Throws<Mixnet.mix_network.IncorrectNumberOfLayersException>(
               delegate
               {
                   MixnetVerifier.verifyMixnet(encryptParams, network, true /* strict */);
               }, "strict verification should have failed");
        }

        /**
         *	Creates a permutation network where each switch gate is connected directly to the next one.
         *  i.e. first output is mapped to the first input of the parallel gate.
         */
        static PermutationNetwork createDirectNetwork(String outputPath, int numLayers, int numInputs, ElGamalEncryptionParameters encryptParams)
        {
            ElGamalPair[,] cipherTexts = new ElGamalPair[numLayers + 1, numInputs];
            int[,] outputIndices = new int[numLayers, numInputs];
            SwitchProof[,] proofs = new SwitchProof[numLayers, numInputs / 2];

            // set inputs
            for (int layer = 0; layer < numLayers; ++layer)
            {
                for (int inputIndex = 0; inputIndex < cipherTexts.GetLength(1); ++inputIndex)
                {
                    cipherTexts[layer, inputIndex] = new ElGamalPair(encryptParams.Group.getZero(),
                                                                     GroupUtils.createRandomElement(encryptParams.Generator, encryptParams.Random));
                }
            }

            for (int layer = 0; layer < numLayers; ++layer)
            {
                for (int switchIndex = 0; switchIndex < cipherTexts.GetLength(1) / 2; ++switchIndex)
                {
                    SwitchingGate gate = new SwitchingGate(encryptParams);
                    gate.InputOne = cipherTexts[layer, switchIndex * 2];
                    gate.InputTwo = cipherTexts[layer, switchIndex * 2 + 1];

                    // does not really matter if we cross or not here.
                    SwitchProof proof = gate.doOperation(false);

                    cipherTexts[layer + 1, switchIndex * 2] = gate.OutputOne;
                    cipherTexts[layer + 1, switchIndex * 2 + 1] = gate.OutputTwo;

                    outputIndices[layer, switchIndex * 2] = switchIndex * 2;
                    outputIndices[layer, switchIndex * 2 + 1] = switchIndex * 2 + 1;

                    proofs[layer, switchIndex] = proof;
                }
            }

            return new PermutationNetwork(encryptParams, cipherTexts, proofs, outputIndices);
        }
    }
}
