﻿using System;
using System.IO;

using NUnit.Framework;
using Org.BouncyCastle.Security;

using Mixnet.crypto;
using Mixnet.mix_network;

namespace UnitTests.mix_network
{
    [TestFixture]
    public class ProverTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();

        [Test]
        public void TestProver()
        {
            String keyInformationFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\ecelgamal.key");
            String proverEncryptionsInputFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"samples\large.enc");

            ElGamalEncryptionParameters encryptParams = MixnetSerializer.loadKeyInformation(keyInformationFilePath);

            PermutationNetwork network = MixnetCreator.createMixnet(encryptParams, proverEncryptionsInputFilePath);

            // verify the result

            Assert.DoesNotThrow(
                delegate
                {
                    MixnetVerifier.verifyMixnet(encryptParams, network, true);
                }, "verification failed");
        }
    }
}
