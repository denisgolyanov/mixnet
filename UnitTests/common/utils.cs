﻿using Mixnet.crypto;
using Mixnet.math;
using Mixnet.math.ec;
using Mixnet.math.misc;
using NUnit.Framework;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Math.EC.Custom.Sec;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Asn1.X9;

namespace UnitTests.common
{
    public class Utils
    {
        public static ElGamalEncryptionParameters createFpCurveParameters(SecureRandom random, out BigInteger privateKey)
        {
            BigInteger n = new BigInteger("6277101735386680763835789423176059013767194773182842284081"); // order
            FpCurve fpCurve = new FpCurve(
                new BigInteger("6277101735386680763835789423207666416083908700390324961279"), // q
                new BigInteger("fffffffffffffffffffffffffffffffefffffffffffffffc", 16), // a
                new BigInteger("64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1", 16), // b
                n,
                BigInteger.One);

            ECGroup curveGroup = new ECGroup(fpCurve);
            ECElement generator = curveGroup.createPoint(
                fpCurve.DecodePoint(Hex.Decode("03188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012")));
            privateKey = new BigInteger("651056770906015076056810763456358567190100156695615665659");
            GroupElement publicKey = generator.multiply(privateKey);

            return new ElGamalEncryptionParameters(publicKey, generator, random);
        }

        public static ElGamalEncryptionParameters createSecp256r1CurveParameters(SecureRandom random, out BigInteger privateKey)
        {

            X9ECParameters a = CustomNamedCurves.GetByName("secp256r1");

            ECGroup curveGroup = new ECGroup(a.Curve);
            ECElement generator = new ECElement(a.G, curveGroup);

            privateKey = new BigInteger("127058840404633923702967028063860590708750029221635");
            GroupElement publicKey = generator.multiply(privateKey);

            return new ElGamalEncryptionParameters(publicKey, generator, random);
        }

        public static ElGamalEncryptionParameters createZnZCurveParameters(SecureRandom random, out BigInteger privateKey)
        {
            AdditiveZGroup group = new AdditiveZGroup(new BigInteger("1000"));
            AdditiveZElement generator = group.createElement(BigInteger.One);
            privateKey = GroupUtils.getRandomIntegerModulusGroupOrder(group, random);
            GroupElement publicKey = generator.multiply(privateKey);

            return new ElGamalEncryptionParameters(publicKey, generator, random);
        }

        public static GroupElement createRandomElement(GroupElement generator, SecureRandom random)
        {
            BigInteger randomK = GroupUtils.getRandomIntegerModulusGroupOrder(generator.getGroup(), random);
            return generator.multiply(randomK);
        }

        public static void AssertElementsEqual(string message, GroupElement a, GroupElement b)
        {
            Assert.AreEqual(a, b, message);
            Assert.AreEqual(b, a, message);
        }

    }
}
