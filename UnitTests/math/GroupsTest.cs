﻿using System;
using System.Collections;

using NUnit.Framework;

using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.EC;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using Mixnet.math;
using Mixnet.math.ec;
using Mixnet.math.misc;
using UnitTests.common;

namespace UnitTests.math
{
    [TestFixture]
    public class GroupsTest
    {
        /**
         * Random source used to generate random points
         */
        private SecureRandom Random = new SecureRandom();

        /**
         * Nested class containing sample literature values for <code>Fp</code>.
         */
        public class Fp
        {
            internal static readonly BigInteger q = new BigInteger("29");
            internal static readonly BigInteger a = new BigInteger("4");
            internal static readonly BigInteger b = new BigInteger("20");
            internal static readonly BigInteger n = new BigInteger("38");
            internal static readonly BigInteger h = new BigInteger("1");

            internal static readonly ECGroup curve = new ECGroup(new FpCurve(q, a, b, n, h));

            internal static readonly int[] pointSource = { 5, 22, 16, 27, 13, 6, 14, 6 };

            internal static ECElement[] p = new ECElement[pointSource.Length / 2];

            /**
             * Creates the points on the curve with literature values.
             */
            internal static void CreatePoints()
            {
                for (int i = 0; i < pointSource.Length / 2; i++)
                {
                    p[i] = curve.createPoint(
                        new BigInteger(pointSource[2 * i].ToString()),
                        new BigInteger(pointSource[2 * i + 1].ToString()));
                }
            }
        }

        /**
         * Nested class containing sample literature values for <code>F2m</code>.
         */
        public class F2m
        {
            // Irreducible polynomial for TPB z^4 + z + 1
            internal const int m = 4;

            internal const int k1 = 1;

            // a = z^3
            internal static readonly BigInteger aTpb = new BigInteger("1000", 2);

            // b = z^3 + 1
            internal static readonly BigInteger bTpb = new BigInteger("1001", 2);

            internal static readonly BigInteger n = new BigInteger("23");

            internal static readonly BigInteger h = new BigInteger("1");

            internal static readonly ECGroup curve = new ECGroup(new F2mCurve(m, k1, aTpb, bTpb, n, h));

            internal static readonly String[] pointSource = { "0010", "1111", "1100", "1100",
                    "0001", "0001", "1011", "0010" };

            internal static readonly ECElement[] p = new ECElement[pointSource.Length / 2];

            /**
             * Creates the points on the curve with literature values.
             */
            internal static void CreatePoints()
            {
                for (int i = 0; i < pointSource.Length / 2; i++)
                {
                    p[i] = curve.createPoint(
                        new BigInteger(pointSource[2 * i], 2),
                        new BigInteger(pointSource[2 * i + 1], 2));
                }
            }
        }

        public class ZnZ
        {
            internal static readonly BigInteger order = new BigInteger("100");

            internal static readonly AdditiveZGroup group = new AdditiveZGroup(order);

            internal static readonly String[] elementSource = { "30", "21", "51", "60",
                    "43", "100", "-10", "100" };

            internal static readonly GroupElement[] p = new GroupElement[elementSource.Length];

            /**
             * Creates the points on the curve with literature values.
             */
            internal static void CreateElements()
            {
                for (int i = 0; i < elementSource.Length; i++)
                {
                    p[i] = group.createElement(new BigInteger(elementSource[i]));
                }
            }
        }

        [SetUp]
        public void SetUp()
        {
            Fp.CreatePoints();
            F2m.CreatePoints();
            ZnZ.CreateElements();
        }

        private void ImplTestAddSubstract(GroupElement[] p, GroupElement zero)
        {
            Utils.AssertElementsEqual("p0 plus p1 does not equal p2", p[2], p[0].add(p[1]));
            Utils.AssertElementsEqual("p1 plus p0 does not equal p2", p[2], p[1].add(p[0]));

            Utils.AssertElementsEqual("p2 minus p1 does not equal p0", p[0], p[2].subtract(p[1]));
            Utils.AssertElementsEqual("p2 minus p0 does not equal p1", p[1], p[2].subtract(p[0]));

            Utils.AssertElementsEqual("p2 plus -p1 does not equal p0", p[0], p[2].add(p[1].negate()));

            for (int i = 0; i < p.Length; i++)
            {
                Utils.AssertElementsEqual("Adding infinity failed", p[i], p[i].add(zero));
                Utils.AssertElementsEqual("Adding to infinity failed", p[i], zero.add(p[i]));
            }
        }

        /**
         *	Tests addition and subtraction on group elements, including zero value.
         */
        [Test]
        public void TestAddSubtract()
        {
            ImplTestAddSubstract(Fp.p, Fp.p[0].getGroup().getZero());
            ImplTestAddSubstract(F2m.p, F2m.p[0].getGroup().getZero());
            ImplTestAddSubstract(ZnZ.p, ZnZ.p[0].getGroup().getZero());
        }

        /**
         * Goes through all points on an elliptic curve and checks, if adding a
         * point <code>k</code>-times is the same as multiplying the point by
         * <code>k</code>, for all <code>k</code>. Should be called for points
         * on very small elliptic curves only.
         *
         * @param p
         *            The base point on the elliptic curve.
         * @param infinity
         *            The point at infinity on the elliptic curve.
         */
        private void ImplTestAllPoints(GroupElement p, GroupElement infinity)
        {
            GroupElement adder = infinity;
            GroupElement multiplier = infinity;

            BigInteger i = BigInteger.One;
            do
            {
                adder = adder.add(p);
                multiplier = p.multiply(i);
                Utils.AssertElementsEqual("Results of Add() and Multiply() are inconsistent " + i, adder, multiplier);
                i = i.Add(BigInteger.One);
            }
            while (!(adder.Equals(infinity)));
        }

        /**
         * Calls <code>implTestAllPoints()</code> for the small literature curves,
         * both for <code>Fp</code> and <code>F2m</code>.
         */
        [Test]
        public void TestAllECPoints()
        {
            for (int i = 0; i < Fp.p.Length; i++)
            {
                ImplTestAllPoints(Fp.p[0], (ECElement)Fp.p[0].getGroup().getZero());
            }

            for (int i = 0; i < F2m.p.Length; i++)
            {
                ImplTestAllPoints(F2m.p[0], (ECElement)F2m.p[0].getGroup().getZero());
            }
        }

        private void ImplTestAddSubtractZero(GroupElement elem)
        {
            GroupElement zero = elem.getGroup().getZero();
            Utils.AssertElementsEqual("elem - elem is not zero", zero, elem.subtract(elem));
            Utils.AssertElementsEqual("elem plus zero is not elem", elem, elem.add(zero));
            Utils.AssertElementsEqual("zero plus elem is not elem", elem, zero.add(elem));
            Utils.AssertElementsEqual("zero plus zero is not zero ", zero, zero.add(zero));
        }

        /**
         *  Addition and subtraction tests relative to the zero element
         */
        [Test]
        public void TestAddSubtractZero()
        {
            for (int iFp = 0; iFp < Fp.pointSource.Length / 2; iFp++)
            {
                ImplTestAddSubtractZero(Fp.p[iFp]);
            }

            for (int iF2m = 0; iF2m < F2m.pointSource.Length / 2; iF2m++)
            {
                ImplTestAddSubtractZero(F2m.p[iF2m]);
            }

            for (int iZnZ = 0; iZnZ < ZnZ.elementSource.Length; iZnZ++)
            {
                ImplTestAddSubtractZero(ZnZ.p[iZnZ]);
            }
        }

        [Test]
        public void TestZnZMod()
        {
            GroupElement element1 = ZnZ.group.createElement(new BigInteger("120"));
            GroupElement element2 = ZnZ.group.createElement(new BigInteger("-80"));
            GroupElement element3 = ZnZ.group.createElement(new BigInteger("20"));

            Utils.AssertElementsEqual("element1 != element2", element1, element2);
            Utils.AssertElementsEqual("element1 != element3", element1, element3);
            Utils.AssertElementsEqual("element2 != element3", element2, element3);
            Utils.AssertElementsEqual("element1 != element3", element1, element1);
        }

    }
}
