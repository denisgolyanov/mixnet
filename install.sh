#!/bin/sh

# After getting the source code manually:

echo "*********** Installing java ***********"
sudo apt-get install openjdk-8-jre
echo "*********** Installing Python ***********"
sudo apt-get install python
echo "*********** Installing pip ***********"
sudo apt install curl
sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py
echo "*********** Installing Tornado ***********"
sudo pip install tornado
echo "*********** Installing Mono ***********"
sudo apt-get install mono-complete
echo "*********** Compile Project ***********"
sudo chmod 777 mixnet/tools/protogen/protogen.exe
sudo xbuild mixnet/Mixnet/Mixnet.csproj
sudo cp -f  mixnet/Mixnet/bin/Debug/Mixnet.exe mixnet/voting_app/server/binaries/Mixnet.exe
echo "*********** Starting Server ***********"
echo "Admin Panel:  http://HOST:8080/admin"
echo "Voting Panel:  http://HOST:8080/"
cd mixnet/voting_app/server/
sudo python main.py
echo "*********** Done ***********"
